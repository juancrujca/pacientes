<?php

/* JCAPacienteBundle:Paciente:messages/error.html.twig */
class __TwigTemplate_e5f99382c517c8ec8ba5315208e9f5fa4c003bb2642d074bf2e02c260496ca17 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e128969a6619695b6e736db83ec02f4497a99833c7684273b7e4012d8164b151 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e128969a6619695b6e736db83ec02f4497a99833c7684273b7e4012d8164b151->enter($__internal_e128969a6619695b6e736db83ec02f4497a99833c7684273b7e4012d8164b151_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:messages/error.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 2
            echo "\t<div class=\"alert alert-danger alert-dismissible\">
\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
\t  <h4><i class=\"icon fa fa-ban\"></i> ";
            // line 4
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h4>
\t</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_e128969a6619695b6e736db83ec02f4497a99833c7684273b7e4012d8164b151->leave($__internal_e128969a6619695b6e736db83ec02f4497a99833c7684273b7e4012d8164b151_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:messages/error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  26 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% for flashMessage in app.session.flashbag.get('error') %}
\t<div class=\"alert alert-danger alert-dismissible\">
\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
\t  <h4><i class=\"icon fa fa-ban\"></i> {{ flashMessage }}</h4>
\t</div>
{% endfor %}", "JCAPacienteBundle:Paciente:messages/error.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/messages/error.html.twig");
    }
}
