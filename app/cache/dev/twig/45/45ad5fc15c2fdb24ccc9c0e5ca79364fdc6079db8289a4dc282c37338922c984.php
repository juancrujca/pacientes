<?php

/* footer.html.twig */
class __TwigTemplate_89265594fc3bb9c0ef47af5def66a7aa45e8d229ae1da4464351e095bfc0e9bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60ce6d1e7fa04953c037b988d745804c8ee17340de1d557c3131b93a25af534c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60ce6d1e7fa04953c037b988d745804c8ee17340de1d557c3131b93a25af534c->enter($__internal_60ce6d1e7fa04953c037b988d745804c8ee17340de1d557c3131b93a25af534c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "footer.html.twig"));

        // line 1
        echo "<footer class=\"main-footer\" id=\"bottom\">
  <div class=\"container\">
    <div class=\"pull-right hidden-xs\">
      <b><a class=\"grey-text text-lighten-4 right\" href=\"https://github.com/JCAlmazan\">GitHub</a></b>
    </div>
    <strong>Copyright &copy; 2017-2018 <a href=\"https://gitlab.com/JCAlmazan\">Juan Cruz Almazán</a>.</strong> All rights
    reserved.
  </div>
  <!-- /.container -->
</footer>";
        
        $__internal_60ce6d1e7fa04953c037b988d745804c8ee17340de1d557c3131b93a25af534c->leave($__internal_60ce6d1e7fa04953c037b988d745804c8ee17340de1d557c3131b93a25af534c_prof);

    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer class=\"main-footer\" id=\"bottom\">
  <div class=\"container\">
    <div class=\"pull-right hidden-xs\">
      <b><a class=\"grey-text text-lighten-4 right\" href=\"https://github.com/JCAlmazan\">GitHub</a></b>
    </div>
    <strong>Copyright &copy; 2017-2018 <a href=\"https://gitlab.com/JCAlmazan\">Juan Cruz Almazán</a>.</strong> All rights
    reserved.
  </div>
  <!-- /.container -->
</footer>", "footer.html.twig", "/home/jca/dev/lemon/pacientes/app/Resources/views/footer.html.twig");
    }
}
