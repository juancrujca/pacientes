<?php

/* JCAPacienteBundle:Paciente:edit.html.twig */
class __TwigTemplate_f12ec4c7798bd7530c338b6726b9531e4362115db9dec8bcf8f2db4f51451bb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "JCAPacienteBundle:Paciente:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d070abf1f312b23077163b1516bcdbfdebb96c818cc04d479ef16b78baa1f965 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d070abf1f312b23077163b1516bcdbfdebb96c818cc04d479ef16b78baa1f965->enter($__internal_d070abf1f312b23077163b1516bcdbfdebb96c818cc04d479ef16b78baa1f965_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d070abf1f312b23077163b1516bcdbfdebb96c818cc04d479ef16b78baa1f965->leave($__internal_d070abf1f312b23077163b1516bcdbfdebb96c818cc04d479ef16b78baa1f965_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_36d8cb5ba0852bce8829b4778dc181d08a0f717bc84da1b67c9616f6bcb7fa74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36d8cb5ba0852bce8829b4778dc181d08a0f717bc84da1b67c9616f6bcb7fa74->enter($__internal_36d8cb5ba0852bce8829b4778dc181d08a0f717bc84da1b67c9616f6bcb7fa74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <div class=\"wrapper\">
    ";
        // line 5
        echo twig_include($this->env, $context, "indexHeader.html.twig");
        echo "
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Editar paciente</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">
              \t";
        // line 17
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => array("novalidate" => "novalidate", "consultorio" => "form")));
        echo "
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "apellidoNombre", array()), 'label', array("label" => "Apellido y nombre"));
        echo "
\t\t\t\t\t\t\t";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "apellidoNombre", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Apellido y nombre del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "apellidoNombre", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<span class=\"hidden consul\" consul_id=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute(($context["consultorio"] ?? $this->getContext($context, "consultorio")), "id", array()), "html", null, true);
        echo "\"></span>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dni", array()), 'label', array("label" => "DNI"));
        echo "
\t\t\t\t\t\t\t";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dni", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "DNI del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dni", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telefono", array()), 'label', array("label" => "Teléfono"));
        echo "
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t                  \t<div class=\"input-group-addon\">
\t\t\t                    \t<i class=\"fa fa-phone\"></i>
\t\t\t                  \t</div>
\t\t\t\t\t\t\t\t";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telefono", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Telefono del paciente")));
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telefono", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaNacimiento", array()), 'label', array("label" => "Fecha de nacimiento"));
        echo "
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\"-->
\t\t                  \t";
        // line 56
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaNacimiento", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Fecha de nacimiento del paciente (ej. 09/02/1885)")));
        echo "       \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaNacimiento", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 62
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "domicilio", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "domicilio", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Domicilio del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 64
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "domicilio", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t";
        // line 68
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaConsulta", array()), 'label', array("label" => "Fecha de la primera consulta"));
        echo "
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\"-->
\t\t                  \t";
        // line 74
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaConsulta", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Fecha de consulta del paciente (ej. 09/02/1885)")));
        echo "       \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 76
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaConsulta", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mc", array()), 'label', array("label" => "Motivo de consulta"));
        echo "
\t\t\t\t\t\t\t";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mc", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Motivo de la consulta del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mc", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 86
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "antecedentes", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 87
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "antecedentes", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Antecedentes del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 88
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "antecedentes", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "medicacion", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "medicacion", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Medicación del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 94
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "medicacion", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>

\t\t\t\t\t<p>
\t\t\t\t\t\t";
        // line 100
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("label" => "Modificar", "attr" => array("class" => "btn btn-success")));
        echo "
\t\t\t\t\t</p>
\t\t\t\t";
        // line 102
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    ";
        // line 113
        echo twig_include($this->env, $context, "footer.html.twig");
        echo "
  </div>
  <!-- ./wrapper -->
";
        
        $__internal_36d8cb5ba0852bce8829b4778dc181d08a0f717bc84da1b67c9616f6bcb7fa74->leave($__internal_36d8cb5ba0852bce8829b4778dc181d08a0f717bc84da1b67c9616f6bcb7fa74_prof);

    }

    // line 118
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d2e07331b7098b898380ef6e6ae85e55e391c597bcaf913f7ff23e00294c951b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2e07331b7098b898380ef6e6ae85e55e391c597bcaf913f7ff23e00294c951b->enter($__internal_d2e07331b7098b898380ef6e6ae85e55e391c597bcaf913f7ff23e00294c951b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 119
        echo "\t
\t";
        // line 120
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t<script>
\t\t\$(document).ready(function(){\t\t
\t\t\tvar consultorioId = \$('.consul').attr(\"consul_id\");
\t    \tvar x = document.getElementsByTagName(\"option\");
\t\t    for(i = 0; i < x.length; i++){
\t\t    \tif(document.getElementsByTagName(\"option\")[i].value == consultorioId){
\t\t    \t\tdocument.getElementsByTagName(\"option\")[i].selected = \"true\";
\t\t   \t\t}
\t\t\t}\t\t    
\t\t});
\t</script>

";
        
        $__internal_d2e07331b7098b898380ef6e6ae85e55e391c597bcaf913f7ff23e00294c951b->leave($__internal_d2e07331b7098b898380ef6e6ae85e55e391c597bcaf913f7ff23e00294c951b_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 120,  274 => 119,  268 => 118,  257 => 113,  243 => 102,  238 => 100,  229 => 94,  225 => 93,  221 => 92,  214 => 88,  210 => 87,  206 => 86,  199 => 82,  195 => 81,  191 => 80,  184 => 76,  179 => 74,  170 => 68,  163 => 64,  159 => 63,  155 => 62,  148 => 58,  143 => 56,  134 => 50,  127 => 46,  122 => 44,  114 => 39,  107 => 35,  103 => 34,  99 => 33,  92 => 29,  88 => 28,  84 => 27,  79 => 25,  73 => 22,  69 => 21,  65 => 20,  59 => 17,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
  <div class=\"wrapper\">
    {{ include('indexHeader.html.twig') }}
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Editar paciente</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">
              \t{{ form_start(form, { 'attr' : {'novalidate' : 'novalidate', 'consultorio' : 'form'} }) }}
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.apellidoNombre, 'Apellido y nombre') }}
\t\t\t\t\t\t\t{{ form_widget(form.apellidoNombre, { 'attr' : {'class' : 'form-control', 'placeholder' : 'Apellido y nombre del paciente'} }) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.apellidoNombre) }}</span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<span class=\"hidden consul\" consul_id=\"{{ consultorio.id }}\"></span>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.consultorio) }}
\t\t\t\t\t\t\t{{ form_widget(form.consultorio, {'attr': {'class': 'form-control'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.consultorio) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.dni, 'DNI') }}
\t\t\t\t\t\t\t{{ form_widget(form.dni, {'attr': {'class': 'form-control', 'placeholder' : 'DNI del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.dni) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.telefono, 'Teléfono') }}
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t                  \t<div class=\"input-group-addon\">
\t\t\t                    \t<i class=\"fa fa-phone\"></i>
\t\t\t                  \t</div>
\t\t\t\t\t\t\t\t{{ form_widget(form.telefono, {'attr': {'class': 'form-control', 'placeholder' : 'Telefono del paciente'}}) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.telefono) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t{{ form_label(form.fechaNacimiento, 'Fecha de nacimiento') }}
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\"-->
\t\t                  \t{{ form_widget(form.fechaNacimiento, {'attr': {'class': 'form-control', 'placeholder' : 'Fecha de nacimiento del paciente (ej. 09/02/1885)'}}) }}       \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.fechaNacimiento) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.domicilio) }}
\t\t\t\t\t\t\t{{ form_widget(form.domicilio, {'attr': {'class': 'form-control', 'placeholder' : 'Domicilio del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.domicilio) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t{{ form_label(form.fechaConsulta, 'Fecha de la primera consulta') }}
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\"-->
\t\t                  \t{{ form_widget(form.fechaConsulta, {'attr': {'class': 'form-control', 'placeholder' : 'Fecha de consulta del paciente (ej. 09/02/1885)'}}) }}       \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.fechaConsulta) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.mc, 'Motivo de consulta') }}
\t\t\t\t\t\t\t{{ form_widget(form.mc, {'attr': {'class': 'form-control', 'placeholder' : 'Motivo de la consulta del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.mc) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.antecedentes) }}
\t\t\t\t\t\t\t{{ form_widget(form.antecedentes, {'attr': {'class': 'form-control', 'placeholder' : 'Antecedentes del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.antecedentes) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.medicacion) }}
\t\t\t\t\t\t\t{{ form_widget(form.medicacion, {'attr': {'class': 'form-control', 'placeholder' : 'Medicación del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.medicacion) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>

\t\t\t\t\t<p>
\t\t\t\t\t\t{{ form_widget(form.save, {'label' : 'Modificar', 'attr': {'class': 'btn btn-success'}}) }}
\t\t\t\t\t</p>
\t\t\t\t{{ form_end(form) }}
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    {{ include('footer.html.twig') }}
  </div>
  <!-- ./wrapper -->
{% endblock %}

{% block javascripts %}
\t
\t{{ parent() }}
\t<script>
\t\t\$(document).ready(function(){\t\t
\t\t\tvar consultorioId = \$('.consul').attr(\"consul_id\");
\t    \tvar x = document.getElementsByTagName(\"option\");
\t\t    for(i = 0; i < x.length; i++){
\t\t    \tif(document.getElementsByTagName(\"option\")[i].value == consultorioId){
\t\t    \t\tdocument.getElementsByTagName(\"option\")[i].selected = \"true\";
\t\t   \t\t}
\t\t\t}\t\t    
\t\t});
\t</script>

{% endblock %}", "JCAPacienteBundle:Paciente:edit.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/edit.html.twig");
    }
}
