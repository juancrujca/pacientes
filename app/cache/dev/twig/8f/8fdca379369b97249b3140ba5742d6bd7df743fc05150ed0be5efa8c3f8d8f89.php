<?php

/* layout.html.twig */
class __TwigTemplate_a63014545a0314e5e7334af0eb4c55e07904a670f9a527187b0879de9217f943 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_355519b665fcd2b4340b8d2cc7223258ddd4c6f3b56c1190df47c8b2d59a12e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_355519b665fcd2b4340b8d2cc7223258ddd4c6f3b56c1190df47c8b2d59a12e5->enter($__internal_355519b665fcd2b4340b8d2cc7223258ddd4c6f3b56c1190df47c8b2d59a12e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 49
        echo "    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class=\"hold-transition skin-blue layout-top-nav\">
        ";
        // line 52
        $this->displayBlock('body', $context, $blocks);
        // line 53
        echo "        
        ";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 85
        echo "    </body>
</html>
";
        
        $__internal_355519b665fcd2b4340b8d2cc7223258ddd4c6f3b56c1190df47c8b2d59a12e5->leave($__internal_355519b665fcd2b4340b8d2cc7223258ddd4c6f3b56c1190df47c8b2d59a12e5_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_e2333aa49f64bde9a173925021a6ac7948315e9ea641b730f0a4e902e89d804f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2333aa49f64bde9a173925021a6ac7948315e9ea641b730f0a4e902e89d804f->enter($__internal_e2333aa49f64bde9a173925021a6ac7948315e9ea641b730f0a4e902e89d804f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Pacientes";
        
        $__internal_e2333aa49f64bde9a173925021a6ac7948315e9ea641b730f0a4e902e89d804f->leave($__internal_e2333aa49f64bde9a173925021a6ac7948315e9ea641b730f0a4e902e89d804f_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_97d8c768531fa5f82e94d6c05edf16ff78fd752aac962314e52f9b493763f688 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97d8c768531fa5f82e94d6c05edf16ff78fd752aac962314e52f9b493763f688->enter($__internal_97d8c768531fa5f82e94d6c05edf16ff78fd752aac962314e52f9b493763f688_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "            <!-- Bootstrap 3.3.7 -->
\t    <link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
            <!-- Font Awesome -->
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
            <!-- Ionicons -->
            <link href=\"https://unpkg.com/ionicons@4.4.8/dist/css/ionicons.min.css\" rel=\"stylesheet\">
            <!-- DataTables -->
            <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css\">
            <!-- Theme style -->
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/css/AdminLTE.min.css\">
            <!-- AdminLTE Skins. Choose a skin from the css/skins
               folder instead of downloading all of them to reduce the load. -->
            <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jcapaciente/css/_all-skins.min.css"), "html", null, true);
        echo "\">

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
            <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
            <![endif]-->

            <!-- Google Font -->
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic\">
            <style>
                .example-modal .modal {
                  position: relative;
                  top: auto;
                  bottom: auto;
                  right: auto;
                  left: auto;
                  display: block;
                  z-index: 1;
                }

                .example-modal .modal {
                  background: transparent !important;
                }
            </style>
        ";
        
        $__internal_97d8c768531fa5f82e94d6c05edf16ff78fd752aac962314e52f9b493763f688->leave($__internal_97d8c768531fa5f82e94d6c05edf16ff78fd752aac962314e52f9b493763f688_prof);

    }

    // line 52
    public function block_body($context, array $blocks = array())
    {
        $__internal_411b58e249052421d00226dbfbffdd7eb12fb2ffa65b3bae3df073c9d91fbfbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_411b58e249052421d00226dbfbffdd7eb12fb2ffa65b3bae3df073c9d91fbfbc->enter($__internal_411b58e249052421d00226dbfbffdd7eb12fb2ffa65b3bae3df073c9d91fbfbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_411b58e249052421d00226dbfbffdd7eb12fb2ffa65b3bae3df073c9d91fbfbc->leave($__internal_411b58e249052421d00226dbfbffdd7eb12fb2ffa65b3bae3df073c9d91fbfbc_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_552295c9eb39890b98d1e8da181499994b495546b068deeba3a054ced7b0e488 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_552295c9eb39890b98d1e8da181499994b495546b068deeba3a054ced7b0e488->enter($__internal_552295c9eb39890b98d1e8da181499994b495546b068deeba3a054ced7b0e488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        echo "       
            <!-- jQuery 3 -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
            <!-- Bootstrap 3.3.7 -->
            <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\t
            <!-- DataTables -->
            <script src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js\"></script>
            <script src=\"https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js\"></script>
            <!-- SlimScroll -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js\"></script>
            <!-- FastClick -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js\"></script>
            <!-- AdminLTE App -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/js/adminlte.min.js\"></script>
            <!-- AdminLTE for demo purposes -->
            <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jcapaciente/js/demo.js"), "html", null, true);
        echo "\"></script>
            <!-- page script -->
            <script>
              \$(function () {
                \$('#example1').DataTable()
                \$('#example2').DataTable({
                  'paging'      : true,
                  'lengthChange': false,
                  'searching'   : false,
                  'ordering'    : true,
                  'info'        : true,
                  'autoWidth'   : false
                })
              })
            </script>
        ";
        
        $__internal_552295c9eb39890b98d1e8da181499994b495546b068deeba3a054ced7b0e488->leave($__internal_552295c9eb39890b98d1e8da181499994b495546b068deeba3a054ced7b0e488_prof);

    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  161 => 69,  139 => 54,  128 => 52,  94 => 22,  80 => 10,  74 => 9,  62 => 6,  53 => 85,  51 => 54,  48 => 53,  46 => 52,  41 => 49,  39 => 9,  33 => 6,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <title>{% block title %}Pacientes{% endblock %}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        {% block stylesheets %}
            <!-- Bootstrap 3.3.7 -->
\t    <link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
            <!-- Font Awesome -->
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
            <!-- Ionicons -->
            <link href=\"https://unpkg.com/ionicons@4.4.8/dist/css/ionicons.min.css\" rel=\"stylesheet\">
            <!-- DataTables -->
            <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css\">
            <!-- Theme style -->
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/css/AdminLTE.min.css\">
            <!-- AdminLTE Skins. Choose a skin from the css/skins
               folder instead of downloading all of them to reduce the load. -->
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/jcapaciente/css/_all-skins.min.css') }}\">

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
            <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
            <![endif]-->

            <!-- Google Font -->
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic\">
            <style>
                .example-modal .modal {
                  position: relative;
                  top: auto;
                  bottom: auto;
                  right: auto;
                  left: auto;
                  display: block;
                  z-index: 1;
                }

                .example-modal .modal {
                  background: transparent !important;
                }
            </style>
        {% endblock %}
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class=\"hold-transition skin-blue layout-top-nav\">
        {% block body %}{% endblock %}
        
        {% block javascripts %}       
            <!-- jQuery 3 -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
            <!-- Bootstrap 3.3.7 -->
            <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\t
            <!-- DataTables -->
            <script src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js\"></script>
            <script src=\"https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js\"></script>
            <!-- SlimScroll -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js\"></script>
            <!-- FastClick -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js\"></script>
            <!-- AdminLTE App -->
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/js/adminlte.min.js\"></script>
            <!-- AdminLTE for demo purposes -->
            <script src=\"{{ asset('bundles/jcapaciente/js/demo.js') }}\"></script>
            <!-- page script -->
            <script>
              \$(function () {
                \$('#example1').DataTable()
                \$('#example2').DataTable({
                  'paging'      : true,
                  'lengthChange': false,
                  'searching'   : false,
                  'ordering'    : true,
                  'info'        : true,
                  'autoWidth'   : false
                })
              })
            </script>
        {% endblock %}
    </body>
</html>
", "layout.html.twig", "/home/jca/dev/lemon/pacientes/app/Resources/views/layout.html.twig");
    }
}
