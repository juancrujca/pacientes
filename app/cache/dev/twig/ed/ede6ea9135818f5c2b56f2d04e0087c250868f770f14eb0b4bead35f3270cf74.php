<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_eb73d465dfccbe9fa02dec94402bf94fef299c95b0b652ba8c7da3db016a5a8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98eca47feea6c5e2b6b8d5358fbca2ec3ec6fe76897aaa81646d40cc77b512d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98eca47feea6c5e2b6b8d5358fbca2ec3ec6fe76897aaa81646d40cc77b512d6->enter($__internal_98eca47feea6c5e2b6b8d5358fbca2ec3ec6fe76897aaa81646d40cc77b512d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_98eca47feea6c5e2b6b8d5358fbca2ec3ec6fe76897aaa81646d40cc77b512d6->leave($__internal_98eca47feea6c5e2b6b8d5358fbca2ec3ec6fe76897aaa81646d40cc77b512d6_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_574e1a6d431421c0573bad9f99490a8b777d6710f794c67ee77558989c8d1d71 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_574e1a6d431421c0573bad9f99490a8b777d6710f794c67ee77558989c8d1d71->enter($__internal_574e1a6d431421c0573bad9f99490a8b777d6710f794c67ee77558989c8d1d71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_574e1a6d431421c0573bad9f99490a8b777d6710f794c67ee77558989c8d1d71->leave($__internal_574e1a6d431421c0573bad9f99490a8b777d6710f794c67ee77558989c8d1d71_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_591c9b1ac37d12a80fd0cd2172ca2a4ef2c7cef8821f743720ca1a85aff915fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_591c9b1ac37d12a80fd0cd2172ca2a4ef2c7cef8821f743720ca1a85aff915fc->enter($__internal_591c9b1ac37d12a80fd0cd2172ca2a4ef2c7cef8821f743720ca1a85aff915fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_591c9b1ac37d12a80fd0cd2172ca2a4ef2c7cef8821f743720ca1a85aff915fc->leave($__internal_591c9b1ac37d12a80fd0cd2172ca2a4ef2c7cef8821f743720ca1a85aff915fc_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_d27b91a602a8cb6e6bcf994b35c1a1063b47881d28f52979b0cbbd0e948f5b02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d27b91a602a8cb6e6bcf994b35c1a1063b47881d28f52979b0cbbd0e948f5b02->enter($__internal_d27b91a602a8cb6e6bcf994b35c1a1063b47881d28f52979b0cbbd0e948f5b02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_d27b91a602a8cb6e6bcf994b35c1a1063b47881d28f52979b0cbbd0e948f5b02->leave($__internal_d27b91a602a8cb6e6bcf994b35c1a1063b47881d28f52979b0cbbd0e948f5b02_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'TwigBundle::layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include 'TwigBundle:Exception:exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/home/jca/dev/lemon/pacientes/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
