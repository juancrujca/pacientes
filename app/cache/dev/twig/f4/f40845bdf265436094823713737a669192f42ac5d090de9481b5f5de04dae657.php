<?php

/* JCAPacienteBundle:Paciente:index.html.twig */
class __TwigTemplate_2d6b54de3d9c796c7fdfc2a5117993a2f20c29b1753d88e7bdcfba2794e14d12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "JCAPacienteBundle:Paciente:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ca1dfb05786166cf05bd4bbf7cb0db57f330b7b6faed7ba34cc88614c24fdc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ca1dfb05786166cf05bd4bbf7cb0db57f330b7b6faed7ba34cc88614c24fdc8->enter($__internal_9ca1dfb05786166cf05bd4bbf7cb0db57f330b7b6faed7ba34cc88614c24fdc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ca1dfb05786166cf05bd4bbf7cb0db57f330b7b6faed7ba34cc88614c24fdc8->leave($__internal_9ca1dfb05786166cf05bd4bbf7cb0db57f330b7b6faed7ba34cc88614c24fdc8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_7a65380ba7570a919961c7876521debabed4bc74b22f2bcf46eb8363e7e91bc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a65380ba7570a919961c7876521debabed4bc74b22f2bcf46eb8363e7e91bc7->enter($__internal_7a65380ba7570a919961c7876521debabed4bc74b22f2bcf46eb8363e7e91bc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <div class=\"wrapper\">
    ";
        // line 5
        echo twig_include($this->env, $context, "indexHeader.html.twig");
        echo "
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      ";
        // line 8
        echo twig_include($this->env, $context, "JCAPacienteBundle:Paciente:messages/succes.html.twig");
        echo "
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Listado de pacientes</h1>
          <div class=\"breadcrumb\">
            <a class=\"btn pull-right\" href=\"#bottom\">
              <i class=\"fa fa-angle-down\"></i> Pie de página
            </a>
          </div>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body table-responsive\">
              <table id=\"example1\" class=\"table table-hover table-bordered table-striped\">
                <thead>
                  <tr>
                    <th>Apellido y nombre</th>
                    <th>Consultorio</th>
                    <th>DNI</th>
                    <th>Edad</th>
                    <th>Teléfono</th>
                    <th><i class=\"fa fa-users\"></i></th>
                  </tr>
                </thead>
                <tbody>
                  ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pacientes"] ?? $this->getContext($context, "pacientes")));
        foreach ($context['_seq'] as $context["_key"] => $context["paciente"]) {
            // line 36
            echo "                    <tr>
                      <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["paciente"], "apellidoNombre", array()), "html", null, true);
            echo "</td>
                      <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["paciente"], "consultorio", array()), "html", null, true);
            echo "</td>
                      <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["paciente"], "dni", array()), "html", null, true);
            echo "</td>
                      <td>";
            // line 40
            echo twig_escape_filter($this->env, (((twig_date_format_filter($this->env, "now", "Y") - twig_date_format_filter($this->env, $this->getAttribute($context["paciente"], "fechaNacimiento", array()), "Y")) - 1) + ((((twig_date_format_filter($this->env, twig_date_format_filter($this->env, "now", "2010-m-d"), "U") - twig_date_format_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["paciente"], "fechaNacimiento", array()), "2010-m-d"), "U")) >= 0)) ? (1) : (0))), "html", null, true);
            echo "</td>
                      <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["paciente"], "telefono", array()), "html", null, true);
            echo "</td>
                      <td class=\"col-sm-1\">
                        <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_view", array("id" => $this->getAttribute($context["paciente"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-block btn-primary btn-social\"><i class=\"fa fa-user\"></i><b>Ver planilla</b></a>
                      </td>
                    </tr>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['paciente'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "                </tbody>
                <tfoot>
                  <tr>
                    <th>Apellido y nombre</th>
                    <th>Consultorio</th>
                    <th>DNI</th>
                    <th>Edad</th>
                    <th>Teléfono</th>
                    <th><i class=\"fa fa-users\"></i></th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    ";
        // line 69
        echo twig_include($this->env, $context, "footer.html.twig");
        echo "
  </div>
  <!-- ./wrapper -->
";
        
        $__internal_7a65380ba7570a919961c7876521debabed4bc74b22f2bcf46eb8363e7e91bc7->leave($__internal_7a65380ba7570a919961c7876521debabed4bc74b22f2bcf46eb8363e7e91bc7_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 69,  117 => 47,  107 => 43,  102 => 41,  98 => 40,  94 => 39,  90 => 38,  86 => 37,  83 => 36,  79 => 35,  49 => 8,  43 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
  <div class=\"wrapper\">
    {{ include('indexHeader.html.twig') }}
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      {{ include('JCAPacienteBundle:Paciente:messages/succes.html.twig') }}
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Listado de pacientes</h1>
          <div class=\"breadcrumb\">
            <a class=\"btn pull-right\" href=\"#bottom\">
              <i class=\"fa fa-angle-down\"></i> Pie de página
            </a>
          </div>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body table-responsive\">
              <table id=\"example1\" class=\"table table-hover table-bordered table-striped\">
                <thead>
                  <tr>
                    <th>Apellido y nombre</th>
                    <th>Consultorio</th>
                    <th>DNI</th>
                    <th>Edad</th>
                    <th>Teléfono</th>
                    <th><i class=\"fa fa-users\"></i></th>
                  </tr>
                </thead>
                <tbody>
                  {% for paciente in pacientes %}
                    <tr>
                      <td>{{ paciente.apellidoNombre }}</td>
                      <td>{{ paciente.consultorio }}</td>
                      <td>{{ paciente.dni }}</td>
                      <td>{{ ('now'|date('Y') - paciente.fechaNacimiento|date('Y') - 1) + ('now'|date('2010-m-d')|date('U') - paciente.fechaNacimiento|date('2010-m-d')|date('U') >= 0 ? 1 : 0) }}</td>
                      <td>{{ paciente.telefono }}</td>
                      <td class=\"col-sm-1\">
                        <a href=\"{{ path('jca_paciente_view', { id: paciente.id }) }}\" class=\"btn btn-block btn-primary btn-social\"><i class=\"fa fa-user\"></i><b>Ver planilla</b></a>
                      </td>
                    </tr>
                  {% endfor %}
                </tbody>
                <tfoot>
                  <tr>
                    <th>Apellido y nombre</th>
                    <th>Consultorio</th>
                    <th>DNI</th>
                    <th>Edad</th>
                    <th>Teléfono</th>
                    <th><i class=\"fa fa-users\"></i></th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    {{ include('footer.html.twig') }}
  </div>
  <!-- ./wrapper -->
{% endblock %}", "JCAPacienteBundle:Paciente:index.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/index.html.twig");
    }
}
