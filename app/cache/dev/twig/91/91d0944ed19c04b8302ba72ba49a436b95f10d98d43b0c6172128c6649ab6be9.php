<?php

/* JCAPacienteBundle:Paciente:addConsultorio.html.twig */
class __TwigTemplate_60f095c233617283df8ed99a7e34b01d4c508358b2dedaa36c17939d521350b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "JCAPacienteBundle:Paciente:addConsultorio.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d9c42e6e69d24219c30775e8101556965b3e2d89cb6d7a82af41c03023cfeaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d9c42e6e69d24219c30775e8101556965b3e2d89cb6d7a82af41c03023cfeaf->enter($__internal_5d9c42e6e69d24219c30775e8101556965b3e2d89cb6d7a82af41c03023cfeaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:addConsultorio.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5d9c42e6e69d24219c30775e8101556965b3e2d89cb6d7a82af41c03023cfeaf->leave($__internal_5d9c42e6e69d24219c30775e8101556965b3e2d89cb6d7a82af41c03023cfeaf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9907089ef97a54ee82e1d43b7a6f3545fcf00bcb77a68f525d8b5f082639df60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9907089ef97a54ee82e1d43b7a6f3545fcf00bcb77a68f525d8b5f082639df60->enter($__internal_9907089ef97a54ee82e1d43b7a6f3545fcf00bcb77a68f525d8b5f082639df60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <div class=\"wrapper\">
    ";
        // line 5
        echo twig_include($this->env, $context, "indexHeader.html.twig");
        echo "
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      ";
        // line 8
        echo twig_include($this->env, $context, "JCAPacienteBundle:Paciente:messages/succes.html.twig");
        echo "
      ";
        // line 9
        echo twig_include($this->env, $context, "JCAPacienteBundle:Paciente:messages/error.html.twig");
        echo "      
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Agregar consultorio</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">
            \t";
        // line 19
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => array("novalidate" => "novalidate")));
        echo "\t\t\t\t\t\t\t\t\t\t\t\t
      \t\t\t\t\t<div class=\"form-group\"> 
      \t\t\t\t\t\t\t";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'label', array("label" => "Consultorio"));
        echo "
      \t\t\t\t\t\t\t";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Agregue el consultorio")));
        echo "
      \t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'errors');
        echo "</span>
      \t\t\t\t\t</div>

      \t\t\t\t\t</fieldset>

      \t\t\t\t\t<p>
      \t\t\t\t\t\t";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("label" => "Agregar", "attr" => array("class" => "btn btn-success")));
        echo "
      \t\t\t\t\t</p>
      \t\t\t\t";
        // line 31
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
              
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->

        <section class=\"content-header\">
          <h1>Eliminar consultorio</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">                                   
                
                <div class=\"form-group\"> 
                    <label>Consultorio</label>
                    <select class=\"form-control\" id=\"seleccion\">
                      <option value=\"\" id=\"predeterminada\">Seleccione un consultorio</option>
                      ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["consultorios"] ?? $this->getContext($context, "consultorios")));
        foreach ($context['_seq'] as $context["_key"] => $context["consultorio"]) {
            // line 53
            echo "                      <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["consultorio"], "id", array()), "html", null, true);
            echo "\" id=\"consultorio";
            echo twig_escape_filter($this->env, $this->getAttribute($context["consultorio"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["consultorio"], "consultorio", array()), "html", null, true);
            echo "</option>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['consultorio'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </select>                    
                </div>               
                          
                <button type=\"button\" class=\"btn btn-danger btn-eliminar\" data-toggle=\"modal\" data-target=\"#modal-eliminar-consultorio\">Eliminar</button>
              
            </div>
            <!-- /.box-body -->
            <!-- Loading-->
            <div class=\"overlay hidden\" id=\"consultorio-progress\">
              <i class=\"fa fa-refresh fa-spin\"></i>
            </div>
          <!-- end loading -->
          </div>
          <!-- /.box -->          

          <!--modal eliminar consultorio-->
            <div class=\"modal modal-danger fade\" id=\"modal-eliminar-consultorio\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">¿Esta seguro que desea eliminar este consultorio?</h4>
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                    ";
        // line 81
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_consultorio_form"] ?? $this->getContext($context, "delete_consultorio_form")), 'form_start', array("attr" => array("id" => "form-delete")));
        echo "

                      ";
        // line 83
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["delete_consultorio_form"] ?? $this->getContext($context, "delete_consultorio_form")), 'widget');
        echo "
                      
                      <button type=\"submit\" class=\"btn btn-outline btn-delete\" value=\"\" data-dismiss=\"modal\">Eliminar</button>
                    
                    ";
        // line 87
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_consultorio_form"] ?? $this->getContext($context, "delete_consultorio_form")), 'form_end');
        echo "
                  </div>
                </div>
              </div>
            </div>
          <!-- /.modal -->

        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    ";
        // line 100
        echo twig_include($this->env, $context, "footer.html.twig");
        echo "
  </div>
  <!-- ./wrapper -->
";
        
        $__internal_9907089ef97a54ee82e1d43b7a6f3545fcf00bcb77a68f525d8b5f082639df60->leave($__internal_9907089ef97a54ee82e1d43b7a6f3545fcf00bcb77a68f525d8b5f082639df60_prof);

    }

    // line 105
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_98c67a4d099871dba12c206d6fbbf628a5c4e5d541b645d98bb2c8ba15b1d19e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98c67a4d099871dba12c206d6fbbf628a5c4e5d541b645d98bb2c8ba15b1d19e->enter($__internal_98c67a4d099871dba12c206d6fbbf628a5c4e5d541b645d98bb2c8ba15b1d19e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 106
        echo "  
  ";
        // line 107
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

  <script> 
    \$('.btn-eliminar').attr(\"disabled\", true);
    \$(\"#jca_pacientebundle_consultorio_consultorio\").val(\"\");
    \$(\"#seleccion\").change(function(){
      \$(\"#predeterminada\").addClass('hidden');
      \$('.btn-eliminar').attr(\"disabled\", false);                  
      var id = \$(this).val();
      \$('.btn-delete').val(id);
    });
  </script>

  <script src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jcapaciente/js/delete-consultorio.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_98c67a4d099871dba12c206d6fbbf628a5c4e5d541b645d98bb2c8ba15b1d19e->leave($__internal_98c67a4d099871dba12c206d6fbbf628a5c4e5d541b645d98bb2c8ba15b1d19e_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:addConsultorio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 120,  211 => 107,  208 => 106,  202 => 105,  191 => 100,  175 => 87,  168 => 83,  163 => 81,  135 => 55,  122 => 53,  118 => 52,  94 => 31,  89 => 29,  80 => 23,  76 => 22,  72 => 21,  67 => 19,  54 => 9,  50 => 8,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
  <div class=\"wrapper\">
    {{ include('indexHeader.html.twig') }}
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      {{ include('JCAPacienteBundle:Paciente:messages/succes.html.twig') }}
      {{ include('JCAPacienteBundle:Paciente:messages/error.html.twig') }}      
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Agregar consultorio</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">
            \t{{ form_start(form, { 'attr' : {'novalidate' : 'novalidate'} }) }}\t\t\t\t\t\t\t\t\t\t\t\t
      \t\t\t\t\t<div class=\"form-group\"> 
      \t\t\t\t\t\t\t{{ form_label(form.consultorio, 'Consultorio') }}
      \t\t\t\t\t\t\t{{ form_widget(form.consultorio, {'attr': {'class': 'form-control', 'placeholder' : 'Agregue el consultorio'}}) }}
      \t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.consultorio) }}</span>
      \t\t\t\t\t</div>

      \t\t\t\t\t</fieldset>

      \t\t\t\t\t<p>
      \t\t\t\t\t\t{{ form_widget(form.save, {'label' : 'Agregar', 'attr': {'class': 'btn btn-success'}}) }}
      \t\t\t\t\t</p>
      \t\t\t\t{{ form_end(form) }}
              
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->

        <section class=\"content-header\">
          <h1>Eliminar consultorio</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">                                   
                
                <div class=\"form-group\"> 
                    <label>Consultorio</label>
                    <select class=\"form-control\" id=\"seleccion\">
                      <option value=\"\" id=\"predeterminada\">Seleccione un consultorio</option>
                      {% for consultorio in consultorios %}
                      <option value=\"{{ consultorio.id }}\" id=\"consultorio{{consultorio.id}}\">{{ consultorio.consultorio }}</option>
                      {% endfor %}
                    </select>                    
                </div>               
                          
                <button type=\"button\" class=\"btn btn-danger btn-eliminar\" data-toggle=\"modal\" data-target=\"#modal-eliminar-consultorio\">Eliminar</button>
              
            </div>
            <!-- /.box-body -->
            <!-- Loading-->
            <div class=\"overlay hidden\" id=\"consultorio-progress\">
              <i class=\"fa fa-refresh fa-spin\"></i>
            </div>
          <!-- end loading -->
          </div>
          <!-- /.box -->          

          <!--modal eliminar consultorio-->
            <div class=\"modal modal-danger fade\" id=\"modal-eliminar-consultorio\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">¿Esta seguro que desea eliminar este consultorio?</h4>
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                    {{ form_start(delete_consultorio_form, {'attr': {'id': 'form-delete'}}) }}

                      {{ form_widget(delete_consultorio_form) }}
                      
                      <button type=\"submit\" class=\"btn btn-outline btn-delete\" value=\"\" data-dismiss=\"modal\">Eliminar</button>
                    
                    {{ form_end(delete_consultorio_form) }}
                  </div>
                </div>
              </div>
            </div>
          <!-- /.modal -->

        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    {{ include('footer.html.twig') }}
  </div>
  <!-- ./wrapper -->
{% endblock %} 

{% block javascripts %}
  
  {{ parent() }}

  <script> 
    \$('.btn-eliminar').attr(\"disabled\", true);
    \$(\"#jca_pacientebundle_consultorio_consultorio\").val(\"\");
    \$(\"#seleccion\").change(function(){
      \$(\"#predeterminada\").addClass('hidden');
      \$('.btn-eliminar').attr(\"disabled\", false);                  
      var id = \$(this).val();
      \$('.btn-delete').val(id);
    });
  </script>

  <script src=\"{{ asset('bundles/jcapaciente/js/delete-consultorio.js') }}\"></script>

{% endblock %}", "JCAPacienteBundle:Paciente:addConsultorio.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/addConsultorio.html.twig");
    }
}
