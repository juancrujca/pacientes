<?php

/* JCAPacienteBundle:Paciente:messages/succes.html.twig */
class __TwigTemplate_c4e2023f9cc46e5f97659d310575b122426adaba1c35d17453a433981e5cd7cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5add7a55bb6e15fe1d5464789ea943b5fb182657d372ae3cd9d1f6a5164b9033 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5add7a55bb6e15fe1d5464789ea943b5fb182657d372ae3cd9d1f6a5164b9033->enter($__internal_5add7a55bb6e15fe1d5464789ea943b5fb182657d372ae3cd9d1f6a5164b9033_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:messages/succes.html.twig"));

        // line 1
        if ( !array_key_exists("flashMessage", $context)) {
            // line 2
            echo "\t<div class=\"alert alert-success hidden alert-dismissible\" id=\"message\" role=\"alert\">
\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
\t  \t<h4><i class=\"icon fa fa-check\"></i> <span id=\"user-message\"></span></h4>
\t</div>
";
        }
        // line 7
        echo "
";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "mensaje"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "\t<div class=\"alert alert-success alert-dismissible\">
\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
\t  <h4><i class=\"icon fa fa-check\"></i> ";
            // line 11
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h4>
\t</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_5add7a55bb6e15fe1d5464789ea943b5fb182657d372ae3cd9d1f6a5164b9033->leave($__internal_5add7a55bb6e15fe1d5464789ea943b5fb182657d372ae3cd9d1f6a5164b9033_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:messages/succes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  38 => 9,  34 => 8,  31 => 7,  24 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if flashMessage is not defined %}
\t<div class=\"alert alert-success hidden alert-dismissible\" id=\"message\" role=\"alert\">
\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
\t  \t<h4><i class=\"icon fa fa-check\"></i> <span id=\"user-message\"></span></h4>
\t</div>
{% endif %}

{% for flashMessage in app.session.flashbag.get('mensaje') %}
\t<div class=\"alert alert-success alert-dismissible\">
\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
\t  <h4><i class=\"icon fa fa-check\"></i> {{ flashMessage }}</h4>
\t</div>
{% endfor %}", "JCAPacienteBundle:Paciente:messages/succes.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/messages/succes.html.twig");
    }
}
