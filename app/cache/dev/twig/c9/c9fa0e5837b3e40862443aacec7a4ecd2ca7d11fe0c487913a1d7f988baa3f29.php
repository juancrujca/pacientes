<?php

/* indexHeader.html.twig */
class __TwigTemplate_5fdd298aa95c7c3a5428a1e3ba941beb5e22b2f6e2852691a0f285d491136c47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a337144a59d51aaadd51e443749843304062636ecc3f35a270d030b9c16d35f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a337144a59d51aaadd51e443749843304062636ecc3f35a270d030b9c16d35f->enter($__internal_8a337144a59d51aaadd51e443749843304062636ecc3f35a270d030b9c16d35f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "indexHeader.html.twig"));

        // line 1
        echo "<header class=\"main-header\">
  <nav class=\"navbar navbar-static-top\">
    <div class=\"container\">
      <div class=\"navbar-header\">
        <a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_index");
        echo "\" class=\"navbar-brand\"><i class=\"fa fa-user-md\"></i><b> PACIENTES</b></a>
        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
          <i class=\"fa fa-bars\"></i>
        </button>
      </div>

      <!-- Navbar Right Menu -->
      <div class=\"collapse navbar-collapse navbar-custom-menu\" id=\"navbar-collapse\">
        <ul class=\"nav navbar-nav\">
          <li><a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_index");
        echo "\"><i class=\"fa fa-table\"></i><b> Listado de pacientes</b></a></li>
          <li><a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_add_consultorio");
        echo "\"><i class=\"fa fa-hospital-o\"></i><b> Gestionar consultorios</b></a></li>
          <li><a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_add");
        echo "\"><i class=\"fa fa-user-plus\"></i><b> Agregar paciente</b></a></li>
      </div>
      <!-- /.navbar-custom-menu -->
    </div>
    <!-- /.container-fluid -->
  </nav>
</header>";
        
        $__internal_8a337144a59d51aaadd51e443749843304062636ecc3f35a270d030b9c16d35f->leave($__internal_8a337144a59d51aaadd51e443749843304062636ecc3f35a270d030b9c16d35f_prof);

    }

    public function getTemplateName()
    {
        return "indexHeader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 16,  44 => 15,  40 => 14,  28 => 5,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"main-header\">
  <nav class=\"navbar navbar-static-top\">
    <div class=\"container\">
      <div class=\"navbar-header\">
        <a href=\"{{ path('jca_paciente_index') }}\" class=\"navbar-brand\"><i class=\"fa fa-user-md\"></i><b> PACIENTES</b></a>
        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
          <i class=\"fa fa-bars\"></i>
        </button>
      </div>

      <!-- Navbar Right Menu -->
      <div class=\"collapse navbar-collapse navbar-custom-menu\" id=\"navbar-collapse\">
        <ul class=\"nav navbar-nav\">
          <li><a href=\"{{ path('jca_paciente_index') }}\"><i class=\"fa fa-table\"></i><b> Listado de pacientes</b></a></li>
          <li><a href=\"{{ path('jca_paciente_add_consultorio') }}\"><i class=\"fa fa-hospital-o\"></i><b> Gestionar consultorios</b></a></li>
          <li><a href=\"{{ path('jca_paciente_add') }}\"><i class=\"fa fa-user-plus\"></i><b> Agregar paciente</b></a></li>
      </div>
      <!-- /.navbar-custom-menu -->
    </div>
    <!-- /.container-fluid -->
  </nav>
</header>", "indexHeader.html.twig", "/home/jca/dev/lemon/pacientes/app/Resources/views/indexHeader.html.twig");
    }
}
