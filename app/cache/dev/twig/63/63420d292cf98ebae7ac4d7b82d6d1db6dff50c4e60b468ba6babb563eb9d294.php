<?php

/* JCAPacienteBundle:Paciente:add.html.twig */
class __TwigTemplate_2b25213e22566cdc622b2d54a7fd8f24361bde8fae8f8769b5e8d93e631a90ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "JCAPacienteBundle:Paciente:add.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac4fa34ae3f65bee6c0b547346e824b6364f2c93525c97db960c763d9e34c706 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac4fa34ae3f65bee6c0b547346e824b6364f2c93525c97db960c763d9e34c706->enter($__internal_ac4fa34ae3f65bee6c0b547346e824b6364f2c93525c97db960c763d9e34c706_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac4fa34ae3f65bee6c0b547346e824b6364f2c93525c97db960c763d9e34c706->leave($__internal_ac4fa34ae3f65bee6c0b547346e824b6364f2c93525c97db960c763d9e34c706_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9240e7e39a15297704e98fd23414f326968c796a2cf7d049251089cc51191027 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9240e7e39a15297704e98fd23414f326968c796a2cf7d049251089cc51191027->enter($__internal_9240e7e39a15297704e98fd23414f326968c796a2cf7d049251089cc51191027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <div class=\"wrapper\">
    ";
        // line 5
        echo twig_include($this->env, $context, "indexHeader.html.twig");
        echo "
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Agregar paciente</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">
              \t";
        // line 17
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => array("novalidate" => "novalidate", "consultorio" => "form")));
        echo "
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "apellidoNombre", array()), 'label', array("label" => "Apellido y nombre"));
        echo "
\t\t\t\t\t\t\t";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "apellidoNombre", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Apellido y nombre del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "apellidoNombre", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "consultorio", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dni", array()), 'label', array("label" => "DNI"));
        echo "
\t\t\t\t\t\t\t";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dni", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "DNI del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dni", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telefono", array()), 'label', array("label" => "Teléfono"));
        echo "
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t                  \t<div class=\"input-group-addon\">
\t\t\t                    \t<i class=\"fa fa-phone\"></i>
\t\t\t                  \t</div>
\t\t\t\t\t\t\t\t";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telefono", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Telefono del paciente")));
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telefono", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaNacimiento", array()), 'label', array("label" => "Fecha de nacimiento"));
        echo "
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\">
\t\t                  \t<!--";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaNacimiento", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Fecha de nacimiento del paciente (ej. 09/02/1885)")));
        echo "-->        \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 57
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaNacimiento", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "domicilio", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 62
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "domicilio", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Ingrese el domicilio del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "domicilio", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaConsulta", array()), 'label', array("label" => "Fecha de la primera consulta"));
        echo "
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\"-->
\t\t                  \t";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaConsulta", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Fecha de consulta del paciente (ej. 09/02/1885)")));
        echo "       \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 75
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fechaConsulta", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 79
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mc", array()), 'label', array("label" => "Motivo de consulta"));
        echo "
\t\t\t\t\t\t\t";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mc", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Motivo de la consulta del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mc", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 85
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "antecedentes", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 86
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "antecedentes", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Antecedentes del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 87
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "antecedentes", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t";
        // line 91
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "medicacion", array()), 'label');
        echo "
\t\t\t\t\t\t\t";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "medicacion", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Medicación del paciente")));
        echo "
\t\t\t\t\t\t\t<span class=\"text-danger\">";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "medicacion", array()), 'errors');
        echo "</span>
\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>

\t\t\t\t\t<p>
\t\t\t\t\t\t";
        // line 99
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-success")));
        echo "
\t\t\t\t\t</p>
\t\t\t\t";
        // line 101
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    ";
        // line 112
        echo twig_include($this->env, $context, "footer.html.twig");
        echo "
  </div>
  <!-- ./wrapper -->
";
        
        $__internal_9240e7e39a15297704e98fd23414f326968c796a2cf7d049251089cc51191027->leave($__internal_9240e7e39a15297704e98fd23414f326968c796a2cf7d049251089cc51191027_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 112,  238 => 101,  233 => 99,  224 => 93,  220 => 92,  216 => 91,  209 => 87,  205 => 86,  201 => 85,  194 => 81,  190 => 80,  186 => 79,  179 => 75,  174 => 73,  165 => 67,  158 => 63,  154 => 62,  150 => 61,  143 => 57,  138 => 55,  129 => 49,  122 => 45,  117 => 43,  109 => 38,  102 => 34,  98 => 33,  94 => 32,  87 => 28,  83 => 27,  79 => 26,  72 => 22,  68 => 21,  64 => 20,  58 => 17,  43 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
  <div class=\"wrapper\">
    {{ include('indexHeader.html.twig') }}
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Agregar paciente</h1>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <!-- /.box-header -->
            <div class=\"box-body\">
              \t{{ form_start(form, { 'attr' : {'novalidate' : 'novalidate', 'consultorio' : 'form'} }) }}
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.apellidoNombre, 'Apellido y nombre') }}
\t\t\t\t\t\t\t{{ form_widget(form.apellidoNombre, { 'attr' : {'class' : 'form-control', 'placeholder' : 'Apellido y nombre del paciente'} }) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.apellidoNombre) }}</span>
\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.consultorio) }}
\t\t\t\t\t\t\t{{ form_widget(form.consultorio, {'attr': {'class': 'form-control'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.consultorio) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.dni, 'DNI') }}
\t\t\t\t\t\t\t{{ form_widget(form.dni, {'attr': {'class': 'form-control', 'placeholder' : 'DNI del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.dni) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.telefono, 'Teléfono') }}
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t                  \t<div class=\"input-group-addon\">
\t\t\t                    \t<i class=\"fa fa-phone\"></i>
\t\t\t                  \t</div>
\t\t\t\t\t\t\t\t{{ form_widget(form.telefono, {'attr': {'class': 'form-control', 'placeholder' : 'Telefono del paciente'}}) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.telefono) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t{{ form_label(form.fechaNacimiento, 'Fecha de nacimiento') }}
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\">
\t\t                  \t<!--{{ form_widget(form.fechaNacimiento, {'attr': {'class': 'form-control', 'placeholder' : 'Fecha de nacimiento del paciente (ej. 09/02/1885)'}}) }}-->        \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.fechaNacimiento) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.domicilio) }}
\t\t\t\t\t\t\t{{ form_widget(form.domicilio, {'attr': {'class': 'form-control', 'placeholder' : 'Ingrese el domicilio del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.domicilio) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t{{ form_label(form.fechaConsulta, 'Fecha de la primera consulta') }}
\t\t\t\t\t\t<div class=\"input-group\">
\t\t                  \t<div class=\"input-group-addon\">
\t\t                    \t<i class=\"fa fa-calendar\"></i>
\t\t                  \t</div>
\t\t                  \t<!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_paciente_fechaNacimiento\" name=\"jca_pacientebundle_paciente[fechaNacimiento]\"-->
\t\t                  \t{{ form_widget(form.fechaConsulta, {'attr': {'class': 'form-control', 'placeholder' : 'Fecha de consulta del paciente (ej. 09/02/1885)'}}) }}       \t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.fechaConsulta) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.mc, 'Motivo de consulta') }}
\t\t\t\t\t\t\t{{ form_widget(form.mc, {'attr': {'class': 'form-control', 'placeholder' : 'Motivo de la consulta del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.mc) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.antecedentes) }}
\t\t\t\t\t\t\t{{ form_widget(form.antecedentes, {'attr': {'class': 'form-control', 'placeholder' : 'Antecedentes del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.antecedentes) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t{{ form_label(form.medicacion) }}
\t\t\t\t\t\t\t{{ form_widget(form.medicacion, {'attr': {'class': 'form-control', 'placeholder' : 'Medicación del paciente'}}) }}
\t\t\t\t\t\t\t<span class=\"text-danger\">{{ form_errors(form.medicacion) }}</span>
\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>

\t\t\t\t\t<p>
\t\t\t\t\t\t{{ form_widget(form.save, {'attr': {'class': 'btn btn-success'}}) }}
\t\t\t\t\t</p>
\t\t\t\t{{ form_end(form) }}
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    {{ include('footer.html.twig') }}
  </div>
  <!-- ./wrapper -->
{% endblock %}", "JCAPacienteBundle:Paciente:add.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/add.html.twig");
    }
}
