<?php

/* JCAPacienteBundle:Paciente:view.html.twig */
class __TwigTemplate_c0e53b3d08bd66b63ab3d38e44f0a60f03dc85889c4cab1ddab7af533e1d51cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "JCAPacienteBundle:Paciente:view.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d8bc532ddfce7ee68b32453ad2be85c9fb22e61d7eb194eea97bde1e999b0d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d8bc532ddfce7ee68b32453ad2be85c9fb22e61d7eb194eea97bde1e999b0d0->enter($__internal_4d8bc532ddfce7ee68b32453ad2be85c9fb22e61d7eb194eea97bde1e999b0d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d8bc532ddfce7ee68b32453ad2be85c9fb22e61d7eb194eea97bde1e999b0d0->leave($__internal_4d8bc532ddfce7ee68b32453ad2be85c9fb22e61d7eb194eea97bde1e999b0d0_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_072da98bcb00a045b3c454204183d7db7e60b328f30cf9a58da3ff88aba4674a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_072da98bcb00a045b3c454204183d7db7e60b328f30cf9a58da3ff88aba4674a->enter($__internal_072da98bcb00a045b3c454204183d7db7e60b328f30cf9a58da3ff88aba4674a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <div class=\"wrapper\">
    ";
        // line 5
        echo twig_include($this->env, $context, "indexHeader.html.twig");
        echo "
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      ";
        // line 8
        echo twig_include($this->env, $context, "JCAPacienteBundle:Paciente:messages/succes.html.twig");
        echo "
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Planilla del paciente</h1>
          <div class=\"breadcrumb\">
            <a class=\"btn pull-right\" href=\"#bottom\">
              <i class=\"fa fa-angle-down\"></i> Última consulta
            </a>
          </div>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <div class=\"box-body row\">
              <!--Nombre y consultorio-->
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Apellido y Nombre: ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "apellidoNombre", array()), "html", null, true);
        echo "</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Consultorio: ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "consultorio", array()), "html", null, true);
        echo "</h5>                 
              </div>
              <!--Edad y telefono-->              
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Edad: ";
        // line 31
        echo twig_escape_filter($this->env, (((twig_date_format_filter($this->env, "now", "Y") - twig_date_format_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "fechaNacimiento", array()), "Y")) - 1) + ((((twig_date_format_filter($this->env, twig_date_format_filter($this->env, "now", "2010-m-d"), "U") - twig_date_format_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "fechaNacimiento", array()), "2010-m-d"), "U")) >= 0)) ? (1) : (0))), "html", null, true);
        echo "</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Telefono: ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "telefono", array()), "html", null, true);
        echo "</h5>  
              </div>
              <!--Dni y Consultorio-->        
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">DNI: ";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "dni", array()), "html", null, true);
        echo "</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Domicilio: ";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "domicilio", array()), "html", null, true);
        echo "</h5>  
              </div>
              <!--Mas datos de consulta-->
              <div class=\"col-sm-12\">         
                <h4>Fecha: ";
        // line 45
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "fechaConsulta", array()), "d/m/Y"), "html", null, true);
        echo "</h4>           
                <h4>MC: ";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "mc", array()), "html", null, true);
        echo "</h4>   
                <h4>Antecedentes: ";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "antecedentes", array()), "html", null, true);
        echo "</h4>          
                <h4>Medicacion: ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "medicacion", array()), "html", null, true);
        echo "</h4>
              </div>     
              <div class=\"btn-group pull-right box-body\">
                <a href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_edit", array("id" => $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "id", array()), "consul" => $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "consultorio", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary btn-social\"><i class=\"fa fa-edit\"></i><b> Editar</b></a>
                <button type=\"button\" class=\"btn btn-danger btn-social\"  data-toggle=\"modal\" data-target=\"#modal-eliminar-paciente\"\">
                  <i class=\"fa fa-remove\"></i><b> Eliminar</b>                  
                </button>
              </div>              
            </div><!--Fin info principal-->
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
          <div class=\"box box-solid\">
            <div class=\"box-header with-border\">
              <h3 class=\"box-title\">Consultas</h3>
              <button type=\"button\" class=\"btn btn-primary pull-right\"  data-toggle=\"modal\" data-target=\"#modal-agregar-consulta\">
                <i class=\"fa fa-plus\"></i><b> Agregar consulta</b>                  
              </button>
            </div>
          </div> 

          <!--modal eliminar paciente-->
          <div class=\"modal modal-danger fade\" id=\"modal-eliminar-paciente\">
            <div class=\"modal-dialog\">
              <div class=\"modal-content\">
                <div class=\"modal-header\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span></button>
                  <h4 class=\"modal-title\">¿Esta seguro que desea eliminar el paciente?</h4>
                </div>
                <div class=\"modal-footer\">
                  <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                  ";
        // line 80
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "

                    ";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'widget');
        echo "
                    
                    <button type=\"submit\" class=\"btn btn-outline\">Eliminar</button>
                  
                  ";
        // line 86
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo " 
                </div>
              </div>
            </div>
          </div>
          <!-- /.modal --> 

          <!--modal agregar consulta-->
            <div class=\"modal fade\" id=\"modal-agregar-consulta\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Agregar consulta</h4>                    
                  </div>
                  <div class=\"modal-body\">
                    
                    ";
        // line 104
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), 'form_start', array("attr" => array("novalidate" => "novalidate", "id" => "form-add")));
        echo "

                    <div class=\"form-group\"> 
                        ";
        // line 107
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "consulta", array()), 'label', array("label" => "Consulta"));
        echo "
                        ";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "consulta", array()), 'widget', array("attr" => array("class" => "form-control textConsultaAdd", "placeholder" => "Consulta")));
        echo "
                        <span class=\"text-danger\">";
        // line 109
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "consulta", array()), 'errors');
        echo "</span>
                    </div>

                    <div class=\"form-group\">
                      ";
        // line 113
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "fechaConsulta", array()), 'label', array("label" => "Fecha de la consulta"));
        echo "
                      <div class=\"input-group\">
                                  <div class=\"input-group-addon\">
                                    <i class=\"fa fa-calendar\"></i>
                                  </div>
                                  <!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_consulta_fechaConsulta\" name=\"jca_pacientebundle_consulta[fechaConsulta]\"-->
                                  ";
        // line 119
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "fechaConsulta", array()), 'widget', array("attr" => array("class" => "form-control dateConsultaAdd", "placeholder" => "Fecha de consulta del paciente (ej. 09/02/1885)")));
        echo "           
                      </div>
                      <span class=\"text-danger\">";
        // line 121
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "fechaConsulta", array()), 'errors');
        echo "</span>
                    </div>
                    <span class=\"label label-info pull-left\">Ambos campos son obligaorios</span> 
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Cerrar</button>

                    <p>
                      ";
        // line 129
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "save", array()), 'widget', array("label" => "Agregar", "attr" => array("class" => "btn btn-success btn-add")));
        echo "
                    </p>

                    ";
        // line 132
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), 'form_end');
        echo "

                  </div>
                </div>              
              </div>
            </div>
          <!-- /.modal -->        

        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    ";
        // line 146
        echo twig_include($this->env, $context, "footer.html.twig");
        echo "
  </div>
  <!-- ./wrapper -->
";
        
        $__internal_072da98bcb00a045b3c454204183d7db7e60b328f30cf9a58da3ff88aba4674a->leave($__internal_072da98bcb00a045b3c454204183d7db7e60b328f30cf9a58da3ff88aba4674a_prof);

    }

    // line 151
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e468ebe5f8acd44e6f730c0d01fb307c21e8165c97f1d212596aec6ea91c198e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e468ebe5f8acd44e6f730c0d01fb307c21e8165c97f1d212596aec6ea91c198e->enter($__internal_e468ebe5f8acd44e6f730c0d01fb307c21e8165c97f1d212596aec6ea91c198e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 152
        echo "
  ";
        // line 153
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
    \$('.btn-add').attr(\"disabled\", true);
    \$('.textConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });
    \$('.dateConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });
  </script>
";
        
        $__internal_e468ebe5f8acd44e6f730c0d01fb307c21e8165c97f1d212596aec6ea91c198e->leave($__internal_e468ebe5f8acd44e6f730c0d01fb307c21e8165c97f1d212596aec6ea91c198e_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 153,  277 => 152,  271 => 151,  260 => 146,  243 => 132,  237 => 129,  226 => 121,  221 => 119,  212 => 113,  205 => 109,  201 => 108,  197 => 107,  191 => 104,  170 => 86,  163 => 82,  158 => 80,  126 => 51,  120 => 48,  116 => 47,  112 => 46,  108 => 45,  101 => 41,  95 => 38,  88 => 34,  82 => 31,  75 => 27,  69 => 24,  50 => 8,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
  <div class=\"wrapper\">
    {{ include('indexHeader.html.twig') }}
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      {{ include('JCAPacienteBundle:Paciente:messages/succes.html.twig') }}
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Planilla del paciente</h1>
          <div class=\"breadcrumb\">
            <a class=\"btn pull-right\" href=\"#bottom\">
              <i class=\"fa fa-angle-down\"></i> Última consulta
            </a>
          </div>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <div class=\"box-body row\">
              <!--Nombre y consultorio-->
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Apellido y Nombre: {{ paciente.apellidoNombre }}</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Consultorio: {{ paciente.consultorio }}</h5>                 
              </div>
              <!--Edad y telefono-->              
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Edad: {{ ('now'|date('Y') - paciente.fechaNacimiento|date('Y') - 1) + ('now'|date('2010-m-d')|date('U') - paciente.fechaNacimiento|date('2010-m-d')|date('U') >= 0 ? 1 : 0) }}</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Telefono: {{ paciente.telefono }}</h5>  
              </div>
              <!--Dni y Consultorio-->        
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">DNI: {{ paciente.dni }}</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Domicilio: {{ paciente.domicilio }}</h5>  
              </div>
              <!--Mas datos de consulta-->
              <div class=\"col-sm-12\">         
                <h4>Fecha: {{ paciente.fechaConsulta|date(\"d/m/Y\") }}</h4>           
                <h4>MC: {{ paciente.mc }}</h4>   
                <h4>Antecedentes: {{ paciente.antecedentes }}</h4>          
                <h4>Medicacion: {{ paciente.medicacion }}</h4>
              </div>     
              <div class=\"btn-group pull-right box-body\">
                <a href=\"{{ path('jca_paciente_edit', { id: paciente.id, consul: paciente.consultorio }) }}\" class=\"btn btn-primary btn-social\"><i class=\"fa fa-edit\"></i><b> Editar</b></a>
                <button type=\"button\" class=\"btn btn-danger btn-social\"  data-toggle=\"modal\" data-target=\"#modal-eliminar-paciente\"\">
                  <i class=\"fa fa-remove\"></i><b> Eliminar</b>                  
                </button>
              </div>              
            </div><!--Fin info principal-->
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
          <div class=\"box box-solid\">
            <div class=\"box-header with-border\">
              <h3 class=\"box-title\">Consultas</h3>
              <button type=\"button\" class=\"btn btn-primary pull-right\"  data-toggle=\"modal\" data-target=\"#modal-agregar-consulta\">
                <i class=\"fa fa-plus\"></i><b> Agregar consulta</b>                  
              </button>
            </div>
          </div> 

          <!--modal eliminar paciente-->
          <div class=\"modal modal-danger fade\" id=\"modal-eliminar-paciente\">
            <div class=\"modal-dialog\">
              <div class=\"modal-content\">
                <div class=\"modal-header\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span></button>
                  <h4 class=\"modal-title\">¿Esta seguro que desea eliminar el paciente?</h4>
                </div>
                <div class=\"modal-footer\">
                  <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                  {{ form_start(delete_form) }}

                    {{ form_widget(delete_form) }}
                    
                    <button type=\"submit\" class=\"btn btn-outline\">Eliminar</button>
                  
                  {{ form_end(delete_form) }} 
                </div>
              </div>
            </div>
          </div>
          <!-- /.modal --> 

          <!--modal agregar consulta-->
            <div class=\"modal fade\" id=\"modal-agregar-consulta\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Agregar consulta</h4>                    
                  </div>
                  <div class=\"modal-body\">
                    
                    {{ form_start(add_consulta_form, { 'attr' : {'novalidate' : 'novalidate', 'id': 'form-add'} }) }}

                    <div class=\"form-group\"> 
                        {{ form_label(add_consulta_form.consulta, 'Consulta') }}
                        {{ form_widget(add_consulta_form.consulta, {'attr': {'class': 'form-control textConsultaAdd', 'placeholder' : 'Consulta'}}) }}
                        <span class=\"text-danger\">{{ form_errors(add_consulta_form.consulta) }}</span>
                    </div>

                    <div class=\"form-group\">
                      {{ form_label(add_consulta_form.fechaConsulta, 'Fecha de la consulta') }}
                      <div class=\"input-group\">
                                  <div class=\"input-group-addon\">
                                    <i class=\"fa fa-calendar\"></i>
                                  </div>
                                  <!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_consulta_fechaConsulta\" name=\"jca_pacientebundle_consulta[fechaConsulta]\"-->
                                  {{ form_widget(add_consulta_form.fechaConsulta, {'attr': {'class': 'form-control dateConsultaAdd', 'placeholder' : 'Fecha de consulta del paciente (ej. 09/02/1885)'}}) }}           
                      </div>
                      <span class=\"text-danger\">{{ form_errors(add_consulta_form.fechaConsulta) }}</span>
                    </div>
                    <span class=\"label label-info pull-left\">Ambos campos son obligaorios</span> 
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Cerrar</button>

                    <p>
                      {{ form_widget(add_consulta_form.save, {'label' : 'Agregar', 'attr': {'class': 'btn btn-success btn-add'}}) }}
                    </p>

                    {{ form_end(add_consulta_form) }}

                  </div>
                </div>              
              </div>
            </div>
          <!-- /.modal -->        

        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    {{ include('footer.html.twig') }}
  </div>
  <!-- ./wrapper -->
{% endblock %}

{% block javascripts %}

  {{ parent() }}
  <script>
    \$('.btn-add').attr(\"disabled\", true);
    \$('.textConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });
    \$('.dateConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });
  </script>
{% endblock %}", "JCAPacienteBundle:Paciente:view.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/view.html.twig");
    }
}
