<?php

/* JCAPacienteBundle:Paciente:viewConsultas.html.twig */
class __TwigTemplate_a111d49e73440c13f25542345ff0ab5a8cf4675d0b490eb2f8e139d490c7b529 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "JCAPacienteBundle:Paciente:viewConsultas.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_90ce073dbd2d077e7019cf9c3cce53b20791ed4eaf2525b54ecd0441f1dd06cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90ce073dbd2d077e7019cf9c3cce53b20791ed4eaf2525b54ecd0441f1dd06cb->enter($__internal_90ce073dbd2d077e7019cf9c3cce53b20791ed4eaf2525b54ecd0441f1dd06cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JCAPacienteBundle:Paciente:viewConsultas.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_90ce073dbd2d077e7019cf9c3cce53b20791ed4eaf2525b54ecd0441f1dd06cb->leave($__internal_90ce073dbd2d077e7019cf9c3cce53b20791ed4eaf2525b54ecd0441f1dd06cb_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ffc32308dafe8dcbaa6ec610bdc75ca43e2e025dd2c58ebf718a334234b47827 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ffc32308dafe8dcbaa6ec610bdc75ca43e2e025dd2c58ebf718a334234b47827->enter($__internal_ffc32308dafe8dcbaa6ec610bdc75ca43e2e025dd2c58ebf718a334234b47827_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <div class=\"wrapper\">
    ";
        // line 5
        echo twig_include($this->env, $context, "indexHeader.html.twig");
        echo "
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      ";
        // line 8
        echo twig_include($this->env, $context, "JCAPacienteBundle:Paciente:messages/succes.html.twig");
        echo "
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Planilla del paciente</h1>
          <div class=\"breadcrumb\">
            <a class=\"btn pull-right\" href=\"#bottom\">
              <i class=\"fa fa-angle-down\"></i> Última consulta
            </a>
          </div>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <div class=\"box-body row\">
              <!--Nombre y consultorio-->
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Apellido y Nombre: ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "apellidoNombre", array()), "html", null, true);
        echo "</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Consultorio: ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "consultorio", array()), "html", null, true);
        echo "</h5>  
              </div>
              <!--Edad y telefono-->              
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Edad: ";
        // line 31
        echo twig_escape_filter($this->env, (((twig_date_format_filter($this->env, "now", "Y") - twig_date_format_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "fechaNacimiento", array()), "Y")) - 1) + ((((twig_date_format_filter($this->env, twig_date_format_filter($this->env, "now", "2010-m-d"), "U") - twig_date_format_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "fechaNacimiento", array()), "2010-m-d"), "U")) >= 0)) ? (1) : (0))), "html", null, true);
        echo "</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Telefono: ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "telefono", array()), "html", null, true);
        echo "</h5>  
              </div>
              <!--Dni y Consultorio-->        
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">DNI: ";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "dni", array()), "html", null, true);
        echo "</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Domicilio: ";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "domicilio", array()), "html", null, true);
        echo "</h5>  
              </div>
              <!--Mas datos de consulta-->
              <div class=\"col-sm-12\">         
                <h4>Fecha: ";
        // line 45
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "fechaConsulta", array()), "d/m/Y"), "html", null, true);
        echo "</h4>           
                <h4>MC: ";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "mc", array()), "html", null, true);
        echo "</h4>   
                <h4>Antecedentes: ";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "antecedentes", array()), "html", null, true);
        echo "</h4>          
                <h4>Medicacion: ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "medicacion", array()), "html", null, true);
        echo "</h4>
              </div> 
              <div class=\"btn-group pull-right box-body\">
                <a href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_edit", array("id" => $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "id", array()), "consul" => $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "consultorio", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary btn-social\"><i class=\"fa fa-edit\"></i><b> Editar</b></a>
                <button type=\"button\" class=\"btn btn-danger btn-social\"  data-toggle=\"modal\" data-target=\"#modal-eliminar-paciente\">
                  <i class=\"fa fa-remove\"></i><b> Eliminar</b>                  
                </button>
              </div>            
            </div><!--Fin info principal-->
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
          <div class=\"box box-solid\">
            <div class=\"box-header with-border\">
              <h3 class=\"box-title\">Consultas</h3>            
              <button type=\"button\" class=\"btn btn-primary pull-right\"  data-toggle=\"modal\" data-target=\"#modal-agregar-consulta\">
                <i class=\"fa fa-plus\"></i><b> Agregar consulta</b>                  
              </button>
            </div>
            ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["consultas"] ?? $this->getContext($context, "consultas")));
        foreach ($context['_seq'] as $context["_key"] => $context["consulta"]) {
            echo "          
              <div class=\"box-body\" id=\"consulta";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\">
                <div class=\"box-group\" id=\"accordion\">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                  <div class=\"panel box box-primary\">
                    <div class=\"box-header with-border\">
                      <h4 class=\"box-title\">
                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\">
                          ";
            // line 75
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["consulta"], "fechaConsulta", array()), "d/m/Y"), "html", null, true);
            echo "
                        </a>
                      </h4>
                    </div>
                    <div id=\"collapseOne";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                      <div class=\"box-body\">
                        <p>";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "consulta", array()), "html", null, true);
            echo "</p>
                        <div class=\"btn-group pull-right\">
                          <!--a href=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jca_paciente_edit_consulta", array("id" => $this->getAttribute($context["consulta"], "id", array()), "paciente" => $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary\"><i class=\"fa fa-edit\"></i><b> Editar</b></a-->
                          <button type=\"button\" class=\"btn btn-primary btn-social btn-modal\"  data-toggle=\"modal\" data-target=\"#modal-editar-consulta\" consultaId=\"";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\" pacienteId=\"";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["paciente"] ?? $this->getContext($context, "paciente")), "id", array()), "html", null, true);
            echo "\" consultaText=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "consulta", array()), "html", null, true);
            echo "\" consultaDate=\"";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["consulta"], "fechaConsulta", array()), "Y-m-d"), "html", null, true);
            echo "\">
                            <i class=\"fa fa-edit\"></i><b> Editar</b>                  
                          </button>                        
                          <button type=\"button\" class=\"btn btn-danger btn-social\"  data-toggle=\"modal\" data-target=\"#modal-eliminar-consulta";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\">
                            <i class=\"fa fa-remove\"></i><b> Eliminar</b>                  
                          </button>
                        </div>
                      </div>
                    </div>
                    <!-- Loading-->
                    <div class=\"overlay hidden\" id=\"consulta-progress";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\">
                      <i class=\"fa fa-refresh fa-spin\"></i>
                    </div>
                    <!-- end loading -->
                  </div>                
                </div>                
              </div>                      

              <!--modal eliminar consulta-->
                <div class=\"modal modal-danger fade\" id=\"modal-eliminar-consulta";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\" consulta-id2>
                  <div class=\"modal-dialog\">
                    <div class=\"modal-content\">
                      <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                          <span aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\">¿Esta seguro que desea eliminar esta consulta?</h4>
                      </div>
                      <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                        ";
            // line 113
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_consulta_form"] ?? $this->getContext($context, "delete_consulta_form")), 'form_start', array("attr" => array("id" => "form-delete")));
            echo "

                          ";
            // line 115
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["delete_consulta_form"] ?? $this->getContext($context, "delete_consulta_form")), 'widget');
            echo "
                          
                          <button type=\"submit\" class=\"btn btn-outline btn-delete\" value=\"";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["consulta"], "id", array()), "html", null, true);
            echo "\" data-dismiss=\"modal\">Eliminar</button>
                        
                        ";
            // line 119
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_consulta_form"] ?? $this->getContext($context, "delete_consulta_form")), 'form_end');
            echo "
                      </div>
                    </div>
                  </div>
                </div>
              <!-- /.modal --> 

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['consulta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "          </div>

          <!--modal editar consulta-->
            <div class=\"modal fade\" id=\"modal-editar-consulta\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Editar consulta</h4>
                  </div>
                  <div class=\"modal-body\">
                    
                    ";
        // line 140
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), 'form_start', array("attr" => array("novalidate" => "novalidate", "id" => "form-edit")));
        echo "

                    <div class=\"form-group\"> 
                        ";
        // line 143
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "consulta", array()), 'label', array("label" => "Consulta"));
        echo "
                        <!--";
        // line 144
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "consulta", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Consulta")));
        echo "-->
                        <textarea id=\"jca_pacientebundle_consulta_consulta\" name=\"jca_pacientebundle_consulta[consulta]\" required=\"required\" class=\"form-control textConsultaEdit\" placeholder=\"Consulta\"></textarea>
                        <span class=\"text-danger\">";
        // line 146
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "consulta", array()), 'errors');
        echo "</span>
                    </div>

                    <div class=\"form-group\">
                      ";
        // line 150
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "fechaConsulta", array()), 'label', array("label" => "Fecha de la consulta"));
        echo "
                      <div class=\"input-group\">
                                  <div class=\"input-group-addon\">
                                    <i class=\"fa fa-calendar\"></i>
                                  </div>
                                  <!--";
        // line 155
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "fechaConsulta", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Fecha de consulta del paciente (ej. 09/02/1885)")));
        echo "-->
                                  <input type=\"date\" id=\"jca_pacientebundle_consulta_fechaConsulta\" name=\"jca_pacientebundle_consulta[fechaConsulta]\" required=\"required\" class=\"form-control dateConsultaEdit\" value=\"\">           
                      </div>
                      <span class=\"text-danger\">";
        // line 158
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "fechaConsulta", array()), 'errors');
        echo "</span>
                    </div>
                    <span class=\"label label-info pull-left\">Ambos campos son obligaorios</span>
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Cerrar</button>

                    <p>
                      <!--";
        // line 166
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), "save", array()), 'widget', array("label" => "Modificar", "attr" => array("class" => "btn btn-success btn-edit")));
        echo "-->
                      <button type=\"submit\" id=\"jca_pacientebundle_consulta_save\" name=\"jca_pacientebundle_consulta[save]\" class=\"btn btn-success btn-edit\" consultaId=\"\" pacienteId=\"\">Modificar</button>
                    </p>                        

                    ";
        // line 170
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_consulta_form"] ?? $this->getContext($context, "edit_consulta_form")), 'form_end');
        echo "

                  </div>
                </div>              
              </div>
            </div>
          <!-- /.modal -->

          <!--modal agregar consulta-->
            <div class=\"modal fade\" id=\"modal-agregar-consulta\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Agregar consulta</h4>
                  </div>
                  <div class=\"modal-body\">
                    
                    ";
        // line 189
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), 'form_start', array("attr" => array("novalidate" => "novalidate", "id" => "form-add")));
        echo "

                    <div class=\"form-group\"> 
                        ";
        // line 192
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "consulta", array()), 'label', array("label" => "Consulta"));
        echo "
                        ";
        // line 193
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "consulta", array()), 'widget', array("attr" => array("class" => "form-control textConsultaAdd", "placeholder" => "Consulta")));
        echo "
                        <span class=\"text-danger\">";
        // line 194
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "consulta", array()), 'errors');
        echo "</span>
                    </div>

                    <div class=\"form-group\">
                      ";
        // line 198
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "fechaConsulta", array()), 'label', array("label" => "Fecha de la consulta"));
        echo "
                      <div class=\"input-group\">
                                  <div class=\"input-group-addon\">
                                    <i class=\"fa fa-calendar\"></i>
                                  </div>
                                  <!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_consulta_fechaConsulta\" name=\"jca_pacientebundle_consulta[fechaConsulta]\"-->
                                  ";
        // line 204
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "fechaConsulta", array()), 'widget', array("attr" => array("class" => "form-control dateConsultaAdd", "placeholder" => "Fecha de consulta del paciente (ej. 09/02/1885)")));
        echo "           
                      </div>
                      <span class=\"text-danger\">";
        // line 206
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "fechaConsulta", array()), 'errors');
        echo "</span>
                    </div>
                    <span class=\"label label-info pull-left\">Ambos campos son obligaorios</span> 
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Cerrar</button>

                    <p>
                      ";
        // line 214
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), "save", array()), 'widget', array("label" => "Agregar", "attr" => array("class" => "btn btn-success btn-add")));
        echo "
                    </p>

                    ";
        // line 217
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["add_consulta_form"] ?? $this->getContext($context, "add_consulta_form")), 'form_end');
        echo "

                  </div>
                </div>              
              </div>
            </div>
          <!-- /.modal --> 

          <!--modal eliminar paciente-->
          <div class=\"modal modal-danger fade\" id=\"modal-eliminar-paciente\">
            <div class=\"modal-dialog\">
              <div class=\"modal-content\">
                <div class=\"modal-header\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span></button>
                  <h4 class=\"modal-title\">¿Esta seguro que desea eliminar el paciente?</h4>
                </div>
                <div class=\"modal-footer\">
                  <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                  ";
        // line 236
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "

                    ";
        // line 238
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'widget');
        echo "
                    
                    <button type=\"submit\" class=\"btn btn-outline\">Eliminar</button>
                  
                  ";
        // line 242
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo " 
                </div>
              </div>
            </div>
          </div>
          <!-- /.modal -->                           

        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    ";
        // line 255
        echo twig_include($this->env, $context, "footer.html.twig");
        echo "
  </div>
  <!-- ./wrapper -->
";
        
        $__internal_ffc32308dafe8dcbaa6ec610bdc75ca43e2e025dd2c58ebf718a334234b47827->leave($__internal_ffc32308dafe8dcbaa6ec610bdc75ca43e2e025dd2c58ebf718a334234b47827_prof);

    }

    // line 260
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_6795c6467e4dd046042290ecfb6546af7140f0faaa483752b5558b12caa4ca0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6795c6467e4dd046042290ecfb6546af7140f0faaa483752b5558b12caa4ca0a->enter($__internal_6795c6467e4dd046042290ecfb6546af7140f0faaa483752b5558b12caa4ca0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 261
        echo "
  ";
        // line 262
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
    \$('.btn-add').attr(\"disabled\", true);
    \$('.textConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });
    \$('.dateConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });

    \$('.textConsultaEdit').change(function(){
      if(\$('.textConsultaEdit').val() == \"\" || \$('.dateConsultaEdit').val() == \"\"){
          \$('.btn-edit').attr(\"disabled\", true);
      }
      else{
          \$('.btn-edit').attr(\"disabled\", false);
      }
    });
    \$('.dateConsultaEdit').change(function(){
      if(\$('.textConsultaEdit').val() == \"\" || \$('.dateConsultaEdit').val() == \"\"){
          \$('.btn-edit').attr(\"disabled\", true);
      }
      else{
          \$('.btn-edit').attr(\"disabled\", false);
      }
    });
  </script>
  <script src=\"";
        // line 299
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jcapaciente/js/delete-consulta.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 300
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jcapaciente/js/edit-consulta.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 301
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jcapaciente/js/modal-edit.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_6795c6467e4dd046042290ecfb6546af7140f0faaa483752b5558b12caa4ca0a->leave($__internal_6795c6467e4dd046042290ecfb6546af7140f0faaa483752b5558b12caa4ca0a_prof);

    }

    public function getTemplateName()
    {
        return "JCAPacienteBundle:Paciente:viewConsultas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  522 => 301,  518 => 300,  514 => 299,  474 => 262,  471 => 261,  465 => 260,  454 => 255,  438 => 242,  431 => 238,  426 => 236,  404 => 217,  398 => 214,  387 => 206,  382 => 204,  373 => 198,  366 => 194,  362 => 193,  358 => 192,  352 => 189,  330 => 170,  323 => 166,  312 => 158,  306 => 155,  298 => 150,  291 => 146,  286 => 144,  282 => 143,  276 => 140,  261 => 127,  247 => 119,  242 => 117,  237 => 115,  232 => 113,  219 => 103,  207 => 94,  197 => 87,  185 => 84,  181 => 83,  176 => 81,  171 => 79,  164 => 75,  160 => 74,  151 => 68,  145 => 67,  126 => 51,  120 => 48,  116 => 47,  112 => 46,  108 => 45,  101 => 41,  95 => 38,  88 => 34,  82 => 31,  75 => 27,  69 => 24,  50 => 8,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
  <div class=\"wrapper\">
    {{ include('indexHeader.html.twig') }}
    <!-- Full Width Column -->
    <div class=\"content-wrapper\">
      {{ include('JCAPacienteBundle:Paciente:messages/succes.html.twig') }}
      <div class=\"container\">
        <!-- Main content -->
        <section class=\"content-header\">
          <h1>Planilla del paciente</h1>
          <div class=\"breadcrumb\">
            <a class=\"btn pull-right\" href=\"#bottom\">
              <i class=\"fa fa-angle-down\"></i> Última consulta
            </a>
          </div>
        </section>
        <section class=\"content\">
          <div class=\"box box-primary\">
            <div class=\"box-body row\">
              <!--Nombre y consultorio-->
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Apellido y Nombre: {{ paciente.apellidoNombre }}</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Consultorio: {{ paciente.consultorio }}</h5>  
              </div>
              <!--Edad y telefono-->              
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">Edad: {{ ('now'|date('Y') - paciente.fechaNacimiento|date('Y') - 1) + ('now'|date('2010-m-d')|date('U') - paciente.fechaNacimiento|date('2010-m-d')|date('U') >= 0 ? 1 : 0) }}</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Telefono: {{ paciente.telefono }}</h5>  
              </div>
              <!--Dni y Consultorio-->        
              <div class=\"col-sm-6\">
                <h3 class=\"text-left\">DNI: {{ paciente.dni }}</h5>  
              </div>
              <div class=\"col-sm-6\">
                <h3 class=\"text-right\">Domicilio: {{ paciente.domicilio }}</h5>  
              </div>
              <!--Mas datos de consulta-->
              <div class=\"col-sm-12\">         
                <h4>Fecha: {{ paciente.fechaConsulta|date(\"d/m/Y\") }}</h4>           
                <h4>MC: {{ paciente.mc }}</h4>   
                <h4>Antecedentes: {{ paciente.antecedentes }}</h4>          
                <h4>Medicacion: {{ paciente.medicacion }}</h4>
              </div> 
              <div class=\"btn-group pull-right box-body\">
                <a href=\"{{ path('jca_paciente_edit', { id: paciente.id, consul: paciente.consultorio  }) }}\" class=\"btn btn-primary btn-social\"><i class=\"fa fa-edit\"></i><b> Editar</b></a>
                <button type=\"button\" class=\"btn btn-danger btn-social\"  data-toggle=\"modal\" data-target=\"#modal-eliminar-paciente\">
                  <i class=\"fa fa-remove\"></i><b> Eliminar</b>                  
                </button>
              </div>            
            </div><!--Fin info principal-->
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
          <div class=\"box box-solid\">
            <div class=\"box-header with-border\">
              <h3 class=\"box-title\">Consultas</h3>            
              <button type=\"button\" class=\"btn btn-primary pull-right\"  data-toggle=\"modal\" data-target=\"#modal-agregar-consulta\">
                <i class=\"fa fa-plus\"></i><b> Agregar consulta</b>                  
              </button>
            </div>
            {% for consulta in consultas %}          
              <div class=\"box-body\" id=\"consulta{{consulta.id}}\">
                <div class=\"box-group\" id=\"accordion\">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                  <div class=\"panel box box-primary\">
                    <div class=\"box-header with-border\">
                      <h4 class=\"box-title\">
                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne{{consulta.id}}\">
                          {{ consulta.fechaConsulta|date(\"d/m/Y\") }}
                        </a>
                      </h4>
                    </div>
                    <div id=\"collapseOne{{consulta.id}}\" class=\"panel-collapse collapse\">
                      <div class=\"box-body\">
                        <p>{{ consulta.consulta }}</p>
                        <div class=\"btn-group pull-right\">
                          <!--a href=\"{{ path('jca_paciente_edit_consulta', { id: consulta.id, paciente: paciente.id }) }}\" class=\"btn btn-primary\"><i class=\"fa fa-edit\"></i><b> Editar</b></a-->
                          <button type=\"button\" class=\"btn btn-primary btn-social btn-modal\"  data-toggle=\"modal\" data-target=\"#modal-editar-consulta\" consultaId=\"{{ consulta.id }}\" pacienteId=\"{{ paciente.id }}\" consultaText=\"{{ consulta.consulta }}\" consultaDate=\"{{ consulta.fechaConsulta|date(\"Y-m-d\") }}\">
                            <i class=\"fa fa-edit\"></i><b> Editar</b>                  
                          </button>                        
                          <button type=\"button\" class=\"btn btn-danger btn-social\"  data-toggle=\"modal\" data-target=\"#modal-eliminar-consulta{{ consulta.id }}\">
                            <i class=\"fa fa-remove\"></i><b> Eliminar</b>                  
                          </button>
                        </div>
                      </div>
                    </div>
                    <!-- Loading-->
                    <div class=\"overlay hidden\" id=\"consulta-progress{{ consulta.id }}\">
                      <i class=\"fa fa-refresh fa-spin\"></i>
                    </div>
                    <!-- end loading -->
                  </div>                
                </div>                
              </div>                      

              <!--modal eliminar consulta-->
                <div class=\"modal modal-danger fade\" id=\"modal-eliminar-consulta{{ consulta.id }}\" consulta-id2>
                  <div class=\"modal-dialog\">
                    <div class=\"modal-content\">
                      <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                          <span aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\">¿Esta seguro que desea eliminar esta consulta?</h4>
                      </div>
                      <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                        {{ form_start(delete_consulta_form, {'attr': {'id': 'form-delete'}}) }}

                          {{ form_widget(delete_consulta_form) }}
                          
                          <button type=\"submit\" class=\"btn btn-outline btn-delete\" value=\"{{ consulta.id }}\" data-dismiss=\"modal\">Eliminar</button>
                        
                        {{ form_end(delete_consulta_form) }}
                      </div>
                    </div>
                  </div>
                </div>
              <!-- /.modal --> 

            {% endfor %}
          </div>

          <!--modal editar consulta-->
            <div class=\"modal fade\" id=\"modal-editar-consulta\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Editar consulta</h4>
                  </div>
                  <div class=\"modal-body\">
                    
                    {{ form_start(edit_consulta_form, { 'attr' : {'novalidate' : 'novalidate', 'id': 'form-edit'} }) }}

                    <div class=\"form-group\"> 
                        {{ form_label(edit_consulta_form.consulta, 'Consulta') }}
                        <!--{{ form_widget(edit_consulta_form.consulta, {'attr': {'class': 'form-control', 'placeholder' : 'Consulta'}}) }}-->
                        <textarea id=\"jca_pacientebundle_consulta_consulta\" name=\"jca_pacientebundle_consulta[consulta]\" required=\"required\" class=\"form-control textConsultaEdit\" placeholder=\"Consulta\"></textarea>
                        <span class=\"text-danger\">{{ form_errors(edit_consulta_form.consulta) }}</span>
                    </div>

                    <div class=\"form-group\">
                      {{ form_label(edit_consulta_form.fechaConsulta, 'Fecha de la consulta') }}
                      <div class=\"input-group\">
                                  <div class=\"input-group-addon\">
                                    <i class=\"fa fa-calendar\"></i>
                                  </div>
                                  <!--{{ form_widget(edit_consulta_form.fechaConsulta, {'attr': {'class': 'form-control', 'placeholder' : 'Fecha de consulta del paciente (ej. 09/02/1885)'}}) }}-->
                                  <input type=\"date\" id=\"jca_pacientebundle_consulta_fechaConsulta\" name=\"jca_pacientebundle_consulta[fechaConsulta]\" required=\"required\" class=\"form-control dateConsultaEdit\" value=\"\">           
                      </div>
                      <span class=\"text-danger\">{{ form_errors(edit_consulta_form.fechaConsulta) }}</span>
                    </div>
                    <span class=\"label label-info pull-left\">Ambos campos son obligaorios</span>
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Cerrar</button>

                    <p>
                      <!--{{ form_widget(edit_consulta_form.save, {'label' : 'Modificar', 'attr': {'class': 'btn btn-success btn-edit'}}) }}-->
                      <button type=\"submit\" id=\"jca_pacientebundle_consulta_save\" name=\"jca_pacientebundle_consulta[save]\" class=\"btn btn-success btn-edit\" consultaId=\"\" pacienteId=\"\">Modificar</button>
                    </p>                        

                    {{ form_end(edit_consulta_form) }}

                  </div>
                </div>              
              </div>
            </div>
          <!-- /.modal -->

          <!--modal agregar consulta-->
            <div class=\"modal fade\" id=\"modal-agregar-consulta\">
              <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                      <span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Agregar consulta</h4>
                  </div>
                  <div class=\"modal-body\">
                    
                    {{ form_start(add_consulta_form, { 'attr' : {'novalidate' : 'novalidate', 'id': 'form-add'} }) }}

                    <div class=\"form-group\"> 
                        {{ form_label(add_consulta_form.consulta, 'Consulta') }}
                        {{ form_widget(add_consulta_form.consulta, {'attr': {'class': 'form-control textConsultaAdd', 'placeholder' : 'Consulta'}}) }}
                        <span class=\"text-danger\">{{ form_errors(add_consulta_form.consulta) }}</span>
                    </div>

                    <div class=\"form-group\">
                      {{ form_label(add_consulta_form.fechaConsulta, 'Fecha de la consulta') }}
                      <div class=\"input-group\">
                                  <div class=\"input-group-addon\">
                                    <i class=\"fa fa-calendar\"></i>
                                  </div>
                                  <!--input type=\"date\" class=\"form-control\" id=\"jca_pacientebundle_consulta_fechaConsulta\" name=\"jca_pacientebundle_consulta[fechaConsulta]\"-->
                                  {{ form_widget(add_consulta_form.fechaConsulta, {'attr': {'class': 'form-control dateConsultaAdd', 'placeholder' : 'Fecha de consulta del paciente (ej. 09/02/1885)'}}) }}           
                      </div>
                      <span class=\"text-danger\">{{ form_errors(add_consulta_form.fechaConsulta) }}</span>
                    </div>
                    <span class=\"label label-info pull-left\">Ambos campos son obligaorios</span> 
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Cerrar</button>

                    <p>
                      {{ form_widget(add_consulta_form.save, {'label' : 'Agregar', 'attr': {'class': 'btn btn-success btn-add'}}) }}
                    </p>

                    {{ form_end(add_consulta_form) }}

                  </div>
                </div>              
              </div>
            </div>
          <!-- /.modal --> 

          <!--modal eliminar paciente-->
          <div class=\"modal modal-danger fade\" id=\"modal-eliminar-paciente\">
            <div class=\"modal-dialog\">
              <div class=\"modal-content\">
                <div class=\"modal-header\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span></button>
                  <h4 class=\"modal-title\">¿Esta seguro que desea eliminar el paciente?</h4>
                </div>
                <div class=\"modal-footer\">
                  <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancelar</button>
                  {{ form_start(delete_form) }}

                    {{ form_widget(delete_form) }}
                    
                    <button type=\"submit\" class=\"btn btn-outline\">Eliminar</button>
                  
                  {{ form_end(delete_form) }} 
                </div>
              </div>
            </div>
          </div>
          <!-- /.modal -->                           

        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    {{ include('footer.html.twig') }}
  </div>
  <!-- ./wrapper -->
{% endblock %}

{% block javascripts %}

  {{ parent() }}
  <script>
    \$('.btn-add').attr(\"disabled\", true);
    \$('.textConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });
    \$('.dateConsultaAdd').change(function(){
      if(\$('.textConsultaAdd').val() == \"\" || \$('.dateConsultaAdd').val() == \"\"){
          \$('.btn-add').attr(\"disabled\", true);
      }
      else{
          \$('.btn-add').attr(\"disabled\", false);
      }
    });

    \$('.textConsultaEdit').change(function(){
      if(\$('.textConsultaEdit').val() == \"\" || \$('.dateConsultaEdit').val() == \"\"){
          \$('.btn-edit').attr(\"disabled\", true);
      }
      else{
          \$('.btn-edit').attr(\"disabled\", false);
      }
    });
    \$('.dateConsultaEdit').change(function(){
      if(\$('.textConsultaEdit').val() == \"\" || \$('.dateConsultaEdit').val() == \"\"){
          \$('.btn-edit').attr(\"disabled\", true);
      }
      else{
          \$('.btn-edit').attr(\"disabled\", false);
      }
    });
  </script>
  <script src=\"{{ asset('bundles/jcapaciente/js/delete-consulta.js') }}\"></script>
  <script src=\"{{ asset('bundles/jcapaciente/js/edit-consulta.js') }}\"></script>
  <script src=\"{{ asset('bundles/jcapaciente/js/modal-edit.js') }}\"></script>

{% endblock %}", "JCAPacienteBundle:Paciente:viewConsultas.html.twig", "/home/jca/dev/lemon/pacientes/src/JCA/PacienteBundle/Resources/views/Paciente/viewConsultas.html.twig");
    }
}
