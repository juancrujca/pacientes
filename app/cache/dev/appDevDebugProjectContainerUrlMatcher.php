<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === rtrim($pathinfo, '/')) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($rawPathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ('/_profiler/purge' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if ('/_configurator' === rtrim($pathinfo, '/')) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($rawPathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ('/_configurator/final' === $pathinfo) {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // jca_paciente_homepage
        if ('' === rtrim($pathinfo, '/')) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($rawPathinfo.'/', 'jca_paciente_homepage');
            }

            return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::indexAction',  '_route' => 'jca_paciente_homepage',);
        }

        if (0 === strpos($pathinfo, '/paciente')) {
            // jca_paciente_index
            if ('/paciente/index' === $pathinfo) {
                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::indexAction',  '_route' => 'jca_paciente_index',);
            }

            // jca_paciente_add
            if ('/paciente/add' === $pathinfo) {
                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::addAction',  '_route' => 'jca_paciente_add',);
            }

            // jca_paciente_create
            if ('/paciente/create' === $pathinfo) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_jca_paciente_create;
                }

                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::createAction',  '_route' => 'jca_paciente_create',);
            }
            not_jca_paciente_create:

            // jca_paciente_edit
            if (0 === strpos($pathinfo, '/paciente/edit') && preg_match('#^/paciente/edit/(?P<id>[^/]++)/(?P<consul>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_edit')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::editAction',));
            }

            // jca_paciente_update
            if (0 === strpos($pathinfo, '/paciente/update') && preg_match('#^/paciente/update/(?P<id>[^/]++)/(?P<consul>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_jca_paciente_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_update')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::updateAction',));
            }
            not_jca_paciente_update:

            // jca_paciente_view
            if (0 === strpos($pathinfo, '/paciente/view') && preg_match('#^/paciente/view/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_view')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::viewAction',));
            }

            // jca_paciente_delete
            if (0 === strpos($pathinfo, '/paciente/delete') && preg_match('#^/paciente/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_jca_paciente_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_delete')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::deleteAction',));
            }
            not_jca_paciente_delete:

            // jca_paciente_redirect_add
            if ('/paciente/create' === $pathinfo) {
                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::addAction',  'path' => '/paciente/add',  'permanent' => true,  '_route' => 'jca_paciente_redirect_add',);
            }

            // jca_paciente_redirect_edit
            if (0 === strpos($pathinfo, '/paciente/update') && preg_match('#^/paciente/update/(?P<id>[^/]++)/(?P<consul>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_redirect_edit')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::editAction',  'path' => '/paciente/edit/{id}/{consul}',  'permanent' => true,));
            }

            // jca_paciente_add_consulta
            if (0 === strpos($pathinfo, '/paciente/addConsulta') && preg_match('#^/paciente/addConsulta/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_jca_paciente_add_consulta;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_add_consulta')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::addConsultaAction',));
            }
            not_jca_paciente_add_consulta:

            // jca_paciente_edit_consulta
            if (0 === strpos($pathinfo, '/paciente/editConsulta') && preg_match('#^/paciente/editConsulta/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_jca_paciente_edit_consulta;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_edit_consulta')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::editConsultaAction',));
            }
            not_jca_paciente_edit_consulta:

            // jca_paciente_delete_consulta
            if (0 === strpos($pathinfo, '/paciente/deleteConsulta') && preg_match('#^/paciente/deleteConsulta/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_jca_paciente_delete_consulta;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_delete_consulta')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::deleteConsultaAction',));
            }
            not_jca_paciente_delete_consulta:

            // jca_paciente_add_consultorio
            if ('/paciente/addConsultorio' === $pathinfo) {
                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::addConsultorioAction',  '_route' => 'jca_paciente_add_consultorio',);
            }

            // jca_paciente_create_consultorio
            if ('/paciente/createConsultorio' === $pathinfo) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_jca_paciente_create_consultorio;
                }

                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::createConsultorioAction',  '_route' => 'jca_paciente_create_consultorio',);
            }
            not_jca_paciente_create_consultorio:

            // jca_paciente_delete_consultorio
            if (0 === strpos($pathinfo, '/paciente/deleteConsultorio') && preg_match('#^/paciente/deleteConsultorio/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_jca_paciente_delete_consultorio;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jca_paciente_delete_consultorio')), array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::deleteConsultorioAction',));
            }
            not_jca_paciente_delete_consultorio:

            // jca_paciente_redirect_add_consultorio
            if ('/paciente/createConsultorio' === $pathinfo) {
                return array (  '_controller' => 'JCA\\PacienteBundle\\Controller\\PacienteController::addConsultorioAction',  'path' => '/paciente/addConsultorio',  'permanent' => true,  '_route' => 'jca_paciente_redirect_add_consultorio',);
            }

        }

        // homepage
        if ('' === rtrim($pathinfo, '/')) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($rawPathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
