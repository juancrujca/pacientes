<?php

namespace JCA\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Consultorio
 *
 * @ORM\Table(name="consultorios")
 * @ORM\Entity(repositoryClass="JCA\PacienteBundle\Entity\ConsultorioRepository")
 */
class Consultorio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="consultorio", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Este campo es obligatorio")
     */
    private $consultorio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consultorio
     *
     * @param string $consultorio
     * @return Consultorio
     */
    public function setConsultorio($consultorio)
    {
        $this->consultorio = $consultorio;

        return $this;
    }

    /**
     * Get consultorio
     *
     * @return string 
     */
    public function getConsultorio()
    {
        return $this->consultorio;
    }

}
