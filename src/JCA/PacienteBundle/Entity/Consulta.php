<?php

namespace JCA\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Symfony\Component\Security\Core\Paciente\PacienteInterface;

/**
 * Consulta
 *
 * @ORM\Table(name="consultas")
 * @ORM\Entity(repositoryClass="JCA\PacienteBundle\Entity\ConsultaRepository")
 */
class Consulta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="consulta", type="text")
     * @Assert\NotBlank(message="Este campo es obligatorio")
     */
    private $consulta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_consulta", type="date")
     * @Assert\NotBlank(message="Este campo es obligatorio")
     */
    private $fechaConsulta;

    /**
     * @var integer
     *
     * @ORM\Column(name="paciente_id", type="integer")
     */
    private $pacienteId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consulta
     *
     * @param string $consulta
     * @return Consulta
     */
    public function setConsulta($consulta)
    {
        $this->consulta = $consulta;

        return $this;
    }

    /**
     * Get consulta
     *
     * @return string
     */
    public function getConsulta()
    {
        return $this->consulta;
    }

    /**
     * Set fechaConsulta
     *
     * @param \DateTime $fechaConsulta
     * @return Consulta
     */
    public function setFechaConsulta($fechaConsulta)
    {
        $this->fechaConsulta = $fechaConsulta;

        return $this;
    }

    /**
     * Get fechaConsulta
     *
     * @return \DateTime 
     */
    public function getFechaConsulta()
    {
        return $this->fechaConsulta;
    }

    /**
     * Set pacienteId
     *
     * @param integer $pacienteId
     * @return Consulta
     */
    public function setPacienteId($pacienteId)
    {
        $this->pacienteId = $pacienteId;

        return $this;
    }

    /**
     * Get pacienteId
     *
     * @return integer 
     */
    public function getPacienteId()
    {
        return $this->pacienteId;
    }
}
