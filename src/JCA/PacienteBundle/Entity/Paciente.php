<?php

namespace JCA\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Symfony\Component\Security\Core\Paciente\PacienteInterface;

/**
 * Paciente
 *
 * @ORM\Table(name="pacientes")
 * @ORM\Entity(repositoryClass="JCA\PacienteBundle\Entity\PacienteRepository")
 */
class Paciente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido_nombre", type="string", length=255)
     * @Assert\NotBlank(message="Este campo es obligatorio")
     */
    private $apellidoNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio", type="string", length=255, nullable=true)
     */
    private $domicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="consultorio", type="string", length=255)
     * @Assert\NotBlank(message="Este campo es obligatorio")
     */
    private $consultorio;

    /**
     * @var bigint
     *
     * @ORM\Column(name="telefono", type="bigint", nullable=true)
     */
    private $telefono;

    /**
     * @var integer
     *
     * @ORM\Column(name="dni", type="integer", nullable=true)
     */
    private $dni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nacimiento", type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_consulta", type="date", nullable=true)
     */
    private $fechaConsulta;

    /**
     * @var string
     *
     * @ORM\Column(name="mc", type="text", nullable=true)
     */
    private $mc;

    /**
     * @var string
     *
     * @ORM\Column(name="antecedentes", type="text", nullable=true)
     */
    private $antecedentes;

    /**
     * @var string
     *
     * @ORM\Column(name="medicacion", type="text", nullable=true)
     */
    private $medicacion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set apellidoNombre
     *
     * @param string $apellidoNombre
     * @return Paciente
     */
    public function setApellidoNombre($apellidoNombre)
    {
        $this->apellidoNombre = $apellidoNombre;

        return $this;
    }

    /**
     * Get apellidoNombre
     *
     * @return string 
     */
    public function getApellidoNombre()
    {
        return $this->apellidoNombre;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     * @return Paciente
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string 
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set consultorio
     *
     * @param string $consultorio
     * @return Paciente
     */
    public function setConsultorio($consultorio)
    {
        $this->consultorio = $consultorio;

        return $this;
    }

    /**
     * Get consultorio
     *
     * @return string 
     */
    public function getConsultorio()
    {
        return $this->consultorio;
    }

    /**
     * Set telefono
     *
     * @param bigint $telefono
     * @return Paciente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return bigint 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set dni
     *
     * @param integer $dni
     * @return Paciente
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return integer 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return Paciente
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set fechaConsulta
     *
     * @param \DateTime $fechaConsulta
     * @return Paciente
     */
    public function setFechaConsulta($fechaConsulta)
    {
        $this->fechaConsulta = $fechaConsulta;

        return $this;
    }

    /**
     * Get fechaConsulta
     *
     * @return \DateTime 
     */
    public function getFechaConsulta()
    {
        return $this->fechaConsulta;
    }

    /**
     * Set mc
     *
     * @param string $mc
     * @return Paciente
     */
    public function setMc($mc)
    {
        $this->mc = $mc;

        return $this;
    }

    /**
     * Get mc
     *
     * @return string 
     */
    public function getMc()
    {
        return $this->mc;
    }

    /**
     * Set antecedentes
     *
     * @param string $antecedentes
     * @return Paciente
     */
    public function setAntecedentes($antecedentes)
    {
        $this->antecedentes = $antecedentes;

        return $this;
    }

    /**
     * Get antecedentes
     *
     * @return string 
     */
    public function getAntecedentes()
    {
        return $this->antecedentes;
    }

    /**
     * Set medicacion
     *
     * @param string $medicacion
     * @return Paciente
     */
    public function setMedicacion($medicacion)
    {
        $this->medicacion = $medicacion;

        return $this;
    }

    /**
     * Get medicacion
     *
     * @return string 
     */
    public function getMedicacion()
    {
        return $this->medicacion;
    }
}
