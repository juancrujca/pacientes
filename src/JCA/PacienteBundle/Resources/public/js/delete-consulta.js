$(document).ready(function(){
	$('.btn-delete').click(function(e){

		e.preventDefault();

		var id = $(this).attr("value");

        //alert(id);

        var form = $('#form-delete');

        var url = form.attr('action').replace(':CONSULTA_ID', id);

        var data = form.serialize();

        $('#consulta-progress'+id).removeClass('hidden');

        $.post(url, data, function(result){

        	$('#consulta-progress'+id).addClass('hidden');

        	if(result.removed == 1)
        	{              			              
       
        		$('#consulta'+id).fadeOut();

        		$('#message').removeClass('hidden');
        		$('#user-message').text(result.message);
        	}
        	else
        	{
        		alert('ERROR');
        		$('#consulta'+id).show();
        	}
        });

        
    });
});