$(document).ready(function(){
	$('.btn-edit').click(function(e){

		e.preventDefault();  

		var consultaId = $(this).attr("consultaId");

        var pacienteId = $(this).attr("pacienteId"); 

        var form = $('#form-edit');

        var url = form.attr('action').replace(':CONSULTA_ID', consultaId);

        var data = form.serialize();

        $('#consulta-progress'+consultaId).removeClass('hidden');

        $.post(url, data, function(result){

        	$('#consulta-progress'+consultaId).addClass('hidden');

        	if(result.edited == 1)
        	{
        		$('#message').removeClass('hidden');
        		$('#user-message').text(result.message);
                                           
                var ref = 'http://pacientes.com/web/app_dev.php/paciente/view/'+pacienteId;          
                window.location.href = ref;              
        	}
        
        });

        
    });
});