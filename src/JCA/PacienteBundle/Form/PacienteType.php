<?php

namespace JCA\PacienteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PacienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellidoNombre')
            ->add('domicilio')
            ->add('consultorio', 'entity', 
                array('class' => 'JCAPacienteBundle:Consultorio', 'choice_label' => 'Consultorio', 'empty_value' => 'Seleccione un consultorio'))
            ->add('telefono')
            ->add('dni')
            ->add('fechaNacimiento', 'date', array('widget' => 'single_text',))
            ->add('fechaConsulta', 'date', array('widget' => 'single_text',))
            ->add('mc')
            ->add('antecedentes')
            ->add('medicacion')
            ->add('save', 'submit', array('label' => 'Agregar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JCA\PacienteBundle\Entity\Paciente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'jca_pacientebundle_paciente';
    }
}
