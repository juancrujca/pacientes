<?php

 namespace JCA\PacienteBundle\Controller;
 
 use Symfony\Bundle\FrameworkBundle\Controller\Controller;
 use Symfony\Component\HttpFoundation\Response;
 use Symfony\Component\HttpFoundation\Request;
 use JCA\PacienteBundle\Entity\Paciente;
 use JCA\PacienteBundle\Form\PacienteType;
 use JCA\PacienteBundle\Entity\Consulta;
 use JCA\PacienteBundle\Form\ConsultaType;
 use JCA\PacienteBundle\Entity\Consultorio;
 use JCA\PacienteBundle\Form\ConsultorioType; 

 class PacienteController extends Controller
 {
     public function indexAction()
     {
        $em = $this->getDoctrine()->getManager();
        
        $pacientes = $em->getRepository('JCAPacienteBundle:Paciente')->findall();
      
        return $this->render('JCAPacienteBundle:Paciente:index.html.twig', array('pacientes' => $pacientes));
    }

    public function addAction()
    {
        $Paciente = new Paciente();
        $form = $this->createCreateForm($Paciente);
        
        return $this->render('JCAPacienteBundle:Paciente:add.html.twig', array('form' => $form->createView()));
    }
    
    private function createCreateForm(Paciente $entity)
    {
        $form = $this->createForm(new PacienteType(), $entity, array(
                'action' => $this->generateUrl('jca_paciente_create'),
                'method' => 'POST'
            ));
        
        return $form;
    }
    
    public function createAction(Request $request)
    {   
        $Paciente = new Paciente();
        $form = $this->createCreateForm($Paciente);
        $form->handleRequest($request);
        
        if($form->isValid())
        {
            /*$password = $form->get('password')->getData();
            
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($Paciente, $password);
            
            $Paciente->setPassword($encoded);
            */
            $em = $this->getDoctrine()->getManager();

        	$consultorio = $em->getRepository('JCAPacienteBundle:Consultorio')->find($form->get('consultorio')->getData());

            $Paciente->setConsultorio($consultorio->getConsultorio());

            $em = $this->getDoctrine()->getManager();
            $em->persist($Paciente);
            $em->flush();

            $this->addFlash('mensaje', 'Se ha agregado el paciente.');
            
            return $this->redirectToRoute('jca_paciente_index');
        }
        
        return $this->render('JCAPacienteBundle:Paciente:add.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, $consul)
    {
        $em = $this->getDoctrine()->getManager();
        $paciente = $em->getRepository('JCAPacienteBundle:Paciente')->find($id);
        
        if(!$paciente)
        {
            throw $this->createNotFoundException('Paciente no encontrado.');
        }

        $consultorio = $em->getRepository('JCAPacienteBundle:Consultorio')->findOneByConsultorio($consul);           

        $form = $this->createEditForm($paciente, $consul);
        
        return $this->render('JCAPacienteBundle:Paciente:edit.html.twig', array('paciente' => $paciente, 'form' => $form->createView(), 'consultorio' => $consultorio ));
        
    }

    private function createEditForm(Paciente $entity, $consul)
    {
    	$form = $this->createForm(new PacienteType(), $entity, array('action' => $this->generateUrl('jca_paciente_update', array('id' => $entity->getId(), 'consul' => $consul)), 'method' => 'PUT' ));

    	return $form;
    }

    public function updateAction($id, $consul, Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	$paciente = $em->getRepository('JCAPacienteBundle:Paciente')->find($id);

        if(!$paciente)
        {
            throw $this->createNotFoundException('Paciente no encontrado.');
        }

        $form = $this->createEditForm($paciente, $consul);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
        	$consultorio = $em->getRepository('JCAPacienteBundle:Consultorio')->find($form->get('consultorio')->getData());

            $paciente->setConsultorio($consultorio->getConsultorio());

        	$em->flush();
        	$this->addFlash('mensaje', 'El paciente ha sido modificado');
        	return $this->redirectToRoute('jca_paciente_view', array('id' => $paciente->getId())); 
        }

        $consultorio = $em->getRepository('JCAPacienteBundle:Consultorio')->findOneByConsultorio($consul); 
        
        return $this->render('JCAPacienteBundle:Paciente:edit.html.twig', array('paciente' => $paciente, 'form' => $form->createView(), 'consultorio' => $consultorio ));
    }
  
    public function viewAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('JCAPacienteBundle:Paciente');
        
        $paciente = $repository->find($id);

        if(!$paciente)
        {            
            throw $this->createNotFoundException('Paciente no encontrado.');
        }

        $em = $this->getDoctrine()->getManager();

		$query = $em->createQuery(
		   'SELECT c
		    FROM JCAPacienteBundle:Consulta c
		    WHERE c.pacienteId = '.$id.'
		    ORDER BY c.fechaConsulta ASC'
		);

		$consultas = $query->getResult();

		$deleteForm = $this->createDeleteForm($paciente);

		$Consulta = new Consulta();

        $addConsultaForm = $this->createConsultaForm($Consulta, $id);
        
        if(!$consultas)
        {
        	return $this->render('JCAPacienteBundle:Paciente:view.html.twig', array('paciente' => $paciente, 'delete_form' => $deleteForm->createView(), 'add_consulta_form' => $addConsultaForm->createView()));
        }

        $deleteConsultaForm = $this->createDeleteConsultaForm(':CONSULTA_ID', 'DELETE', 'jca_paciente_delete_consulta');

        $editConsultaForm = $this->createEditConsultaForm(':CONSULTA_ID', 'PUT', 'jca_paciente_edit_consulta');        

        return $this->render('JCAPacienteBundle:Paciente:viewConsultas.html.twig', array('paciente' => $paciente, 'consultas' => $consultas, 'delete_form' => $deleteForm->createView(), 'delete_consulta_form' => $deleteConsultaForm->createView(), 'edit_consulta_form' => $editConsultaForm->createView(), 'add_consulta_form' => $addConsultaForm->createView()));
    }

    private function createEditConsultaForm($id, $method, $route)
    {	
    	$em = $this->getDoctrine()->getManager();

    	$entity = $em->getRepository('JCAPacienteBundle:Consulta')->find($id);  	

    	$form = $this->createForm(new ConsultaType(), $entity, array('action' => $this->generateUrl('jca_paciente_edit_consulta', array('id' => $id)), 'method' => $method ));

    	return $form;
    }

    public function editConsultaAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$consulta = $em->getRepository('JCAPacienteBundle:Consulta')->find($id);

        if(!$consulta)
        {
            throw $this->createNotFoundException('Consulta no encontrada.');
        }

        $form = $this->createEditConsultaForm($id, 'PUT', 'jca_paciente_delete_consulta');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
        	if($request->isXMLHttpRequest())
        	{        		
            	$em->flush();
            	$message = 'La consulta ha sido modificada.';
            	$edited = 1;

            	return new Response(
            		json_encode(array('edited' => $edited, 'message'=> $message )),
            		200,
            		array('Content-Type' => 'application/json')
            	);
        	}        	
        }
    }

    private function createDeleteConsultaForm($id, $method, $route)
    {    	
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl($route, array('id' => $id)))
    	->setMethod($method)
    	->getForm();
    }

    public function deleteConsultaAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('JCAPacienteBundle:Consulta')->find($id);
        
        if(!$consulta)
        {            
            throw $this->createNotFoundException('Consulta no encontrada.');
        }      

		$form = $this->createDeleteConsultaForm($id, 'DELETE', 'jca_paciente_delete_consulta');
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {	

        	if($request->isXMLHttpRequest())
        	{
        		$em->remove($consulta);
            	$em->flush();
            	$message = 'La consulta ha sido eliminada.';
            	$removed = 1;

            	return new Response(
            		json_encode(array('removed' => $removed, 'message'=> $message )),
            		200,
            		array('Content-Type' => 'application/json')
            	);
        	}        	                        
           	        
        }            
    }

    private function createDeleteForm($paciente)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('jca_paciente_delete', array('id' => $paciente->getId())))
    	->setMethod('DELETE')
    	->getForm();
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $paciente = $em->getRepository('JCAPacienteBundle:Paciente')->find($id);

        $consultas = $em->getRepository('JCAPacienteBundle:Consulta')->findByPacienteId($id);
        
        if(!$paciente)
        {            
            throw $this->createNotFoundException('Paciente no encontrado.');
        }
        
        $form = $this->createDeleteForm($paciente);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {	
        	foreach ($consultas as $consulta) 
        	{
    			$em->remove($consulta);
			}
            $em->remove($paciente);
            $em->flush();
                        
            $this->addFlash('mensaje', 'El paciente ha sido eliminado.');
            return $this->redirectToRoute('jca_paciente_index');            
        }
    }    
    
    private function createConsultaForm(Consulta $entity, $id)
    {

    	$form = $this->createForm(new ConsultaType(), $entity, array('action' => $this->generateUrl('jca_paciente_add_consulta', array('id' => $id)), 'method' => 'POST' ));

    	return $form;        
    }
    
    public function addConsultaAction($id, Request $request)
    {   
        $Consulta = new Consulta();
        $form = $this->createConsultaForm($Consulta, $id);
        $form->handleRequest($request);
        
        if($form->isValid())
        {
            
            $Consulta->setPacienteId($id);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Consulta);
            $em->flush();

            $this->addFlash('mensaje', 'Se ha agregado la consulta.');
            
            return $this->redirectToRoute('jca_paciente_view', array('id' => $id));
        }
        
        return $this->redirectToRoute('jca_paciente_view', array('id' => $id));
    }
     
    public function addConsultorioAction()
    {
        $Consultorio = new Consultorio();
        $form = $this->createCreateConsultorioForm($Consultorio);

        $deleteConsultorioForm = $this->createDeleteConsultorioForm(':CONSULTORIO_ID', 'DELETE', 'jca_paciente_delete_consultorio');

        $em = $this->getDoctrine()->getManager();

        $consultorios = $em->getRepository('JCAPacienteBundle:Consultorio')->findBy(array(), array('consultorio' => 'ASC'));
        
        return $this->render('JCAPacienteBundle:Paciente:addConsultorio.html.twig', array('form' => $form->createView(), 'delete_consultorio_form' => $deleteConsultorioForm->createView(), 'consultorios' => $consultorios));
    }

    private function createDeleteConsultorioForm($id, $method, $route)
    {       
        return $this->createFormBuilder()
        ->setAction($this->generateUrl($route, array('id' => $id)))
        ->setMethod($method)
        ->getForm();
    }

    public function deleteConsultorioAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $consultorio = $em->getRepository('JCAPacienteBundle:Consultorio')->find($id);
        
        if(!$consultorio)
        {            
            throw $this->createNotFoundException('Consultorio no encontrado.');
        }      

        $form = $this->createDeleteConsultorioForm($id, 'DELETE', 'jca_paciente_delete_consultorio');
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {   

            if($request->isXMLHttpRequest())
            {
                $em->remove($consultorio);
                $em->flush();
                $message = 'El consultorio ha sido eliminado.';
                $removed = 1;

                return new Response(
                    json_encode(array('removed' => $removed, 'message'=> $message )),
                    200,
                    array('Content-Type' => 'application/json')
                );
            }                                   
                    
        }            
    }
    
    private function createCreateConsultorioForm(Consultorio $entity)
    {
        $form = $this->createForm(new ConsultorioType(), $entity, array(
                'action' => $this->generateUrl('jca_paciente_create_consultorio'),
                'method' => 'POST'
            ));
        
        return $form;
    }
    
    public function createConsultorioAction(Request $request)
    {   
        $Consultorio = new Consultorio();
        $form = $this->createCreateConsultorioForm($Consultorio);
        $form->handleRequest($request);        
        
        if($form->isValid())
        {
        	$em = $this->getDoctrine()->getManager();
    		$existe = $em->getRepository('JCAPacienteBundle:Consultorio')->findByConsultorio($Consultorio->getConsultorio());

    		if(!$existe)
    		{	            
	            $em->persist($Consultorio);
	            $em->flush();

	            $this->addFlash('mensaje', 'Se ha agregado el consultorio.');

                $deleteConsultorioForm = $this->createDeleteConsultorioForm(':CONSULTORIO_ID', 'DELETE', 'jca_paciente_delete_consultorio');

                $em = $this->getDoctrine()->getManager();

                $consultorios = $em->getRepository('JCAPacienteBundle:Consultorio')->findBy(array(), array('consultorio' => 'ASC'));
	            
	            return $this->render('JCAPacienteBundle:Paciente:addConsultorio.html.twig', array('form' => $form->createView(), 'delete_consultorio_form' => $deleteConsultorioForm->createView(), 'consultorios' => $consultorios));
	        }
	        
            $this->addFlash('error', 'El consultorio ya existe.');
        }

        $deleteConsultorioForm = $this->createDeleteConsultorioForm(':CONSULTORIO_ID', 'DELETE', 'jca_paciente_delete_consultorio');

        $em = $this->getDoctrine()->getManager();

        $consultorios = $em->getRepository('JCAPacienteBundle:Consultorio')->findBy(array(), array('consultorio' => 'ASC'));   

        return $this->render('JCAPacienteBundle:Paciente:addConsultorio.html.twig', array('form' => $form->createView(), 'delete_consultorio_form' => $deleteConsultorioForm->createView(), 'consultorios' => $consultorios));
    }

}