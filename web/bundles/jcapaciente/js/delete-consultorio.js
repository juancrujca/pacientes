$(document).ready(function(){
	$('.btn-delete').click(function(e){

		e.preventDefault();

		var id = $(this).attr("value");

        //alert(id);

        var form = $('#form-delete');

        var url = form.attr('action').replace(':CONSULTORIO_ID', id);

        var data = form.serialize();

        $('#consultorio-progress').removeClass('hidden');

        $.post(url, data, function(result){

        	$('#consultorio-progress').addClass('hidden');

        	if(result.removed == 1)
        	{              			              
       
        		$('#consultorio'+id).fadeOut();
                $("#seleccion").val("");

        		$('#message').removeClass('hidden');
        		$('#user-message').text(result.message);


        	}
        	else
        	{
        		alert('ERROR');
        		$('#consultorio'+id).show();
        	}
        });

        
    });
});