$(document).ready(function(){
	$('.btn-modal').click(function(e){

		e.preventDefault();  

		var consultaId = $(this).attr("consultaId");

        var pacienteId = $(this).attr("pacienteId");

        $("#jca_pacientebundle_consulta_save").attr("consultaId", consultaId);

        $("#jca_pacientebundle_consulta_save").attr("pacienteId", pacienteId);

        var text = $(this).attr("consultaText");

        var date = $(this).attr("consultaDate");

        $("#jca_pacientebundle_consulta_consulta").text(text);

        $("#jca_pacientebundle_consulta_fechaConsulta").val(date)

        
    });
});